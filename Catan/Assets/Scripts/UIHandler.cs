using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using System;

public class UIHandler : MonoBehaviour
{
    private Board board;

    /*-----------------------------------------------------------------------*/
    /*---------------------------[Text References]---------------------------*/
    /*-----------------------------------------------------------------------*/
    public TextMeshProUGUI[] playerNames = new TextMeshProUGUI[4];
    public TextMeshProUGUI[] playerPoints = new TextMeshProUGUI[4];
    public TextMeshProUGUI[] playerRoadlength = new TextMeshProUGUI[4];
    public TextMeshProUGUI[] playerKnightsUsed = new TextMeshProUGUI[4];
    public TextMeshProUGUI[] playerResourceCards = new TextMeshProUGUI[4];

    public TextMeshProUGUI[] structureAmounts = new TextMeshProUGUI[3];
    // Streets, Settlements, Cities

    public TextMeshProUGUI[] resourceAmounts = new TextMeshProUGUI[5];
    // Wood, Stone, Wool, Wheat, Ore

    public TextMeshProUGUI[] devCardAmounts = new TextMeshProUGUI[9];
    // Chapel, Great Hall, Library, Market, University, Knight, Monopoly, Year of Plenty, Road Building

    public TextMeshProUGUI popupText;
    public TextMeshProUGUI popupHeaderText;

    /*----------------------------------------------------------------------*/
    /*--------------------------[Other References]--------------------------*/
    /*----------------------------------------------------------------------*/
    public GameObject popupHeader;  

    public GameObject popupInstructions;

    public GameObject expandedResourcePanel;
    public GameObject expandableResourcePanel;

    public GameObject[] phases = new GameObject[3];
    // Dice, Trading, Building

    public GameObject[] portChecks = new GameObject[6];
    // Three, Wood, Stone, Wool, Wheat, Ore

    public DiceLarge[] dice = new DiceLarge[2];

    public GameObject[] diceSmall = new GameObject[2];

    public GameObject[] panels = new GameObject[3];
    // Dice Throwing | Trading | Development

    public GameObject[] devActionPanels = new GameObject[2];
    // Monopoly | Plenty

    public GameObject[] panelHeaders = new GameObject[3];

    public GameObject[] devActionHeaders = new GameObject[2];

    public GameObject popupPanel;

    public GameObject[] throwableDice = new GameObject[2];

    public Toggle testGame;
    public Toggle[] AI = new Toggle[4];
    public Toggle randomResources;

    public TMP_InputField[] nameImputs = new TMP_InputField[4];
    // Player 1, 2, 3, 4

    public FlexibleColorPicker[] colorPickers = new FlexibleColorPicker[4];
    // Player 1, 2, 3, 4

    public GameObject[] playerColorPanels = new GameObject[4];

    public GameObject menu;

    public GameObject robber;

    public GameObject confirmButton;

    public GameObject rerollButton;

    public ViewReset viewResetButton;

    public OpenCardWindow openCardWindowButton;

    public GameObject buildSettlement;
    public GameObject buildRoad;

    public DieSideChecker dsc;

    public TradeManager tm;

    // General info about each player displayed at the top
    public Image[] playerVictoryIcons = new Image[4];
    public Image[] playerResourcesIcons = new Image[4];
    public Image[] playerKnightsIcons = new Image[4];
    public Image[] playerRoadIcons = new Image[4];

    /*----------------------------------------------------------------------*/
    /*---------------------------[Trade Proposal]---------------------------*/
    /*----------------------------------------------------------------------*/

    public GameObject tradeProposal;
    public GameObject tradeProposalHeader;

    public GameObject tradeProposalVersion;
    public GameObject tradeConfirmationVersion;

    // Wheat, Wood, Stone, Wool, Ore
    public GameObject[] iFieldsGive = new GameObject[5];
    public GameObject[] iFieldsReceive = new GameObject[5];
    
    public ProposalConfirmation proposalConfirmButton;
    public ProposalConfirmation proposalDeclineButton;

    public GameObject[] playerButtons = new GameObject[3];
    public PlayerButton[] playerButtonScripts = new PlayerButton[3];
    public TextMeshProUGUI[] playerButtonNames = new TextMeshProUGUI[3];
    public GameObject[] resourcesGive = new GameObject[5];
    public GameObject[] resourcesReceive = new GameObject[5];
    // Wheat, Wood, Stone, Wool, Ore

    public GameObject pm;

    /*----------------------------------------------------------------------*/
    /*------------------[IEnumerators that need MonoBehaviour]--------------*/
    /*----------------------------------------------------------------------*/

    public WaitForSeconds wfs = new WaitForSeconds(0.1f);  // Setting standard delay
    
    private void Awake()
    {
        board = GetComponent<Board>();
    }

    public IEnumerator ActivateRerollButton()
    {
        yield return new WaitForSeconds(10);
        if (dsc.dice.Count > 0)
            rerollButton.SetActive(true);
    }

    public IEnumerator GiveDiceSides(int die1, int die2)
    {
        yield return new WaitForSeconds(gameObject.GetComponent<Board>().playerAtTurn.ui.dice[0].lerpDuration);
        gameObject.GetComponent<Board>().GetDiceSides(die1, die2);
    }

    public IEnumerator ShowPopupPanel()
    {
        yield return wfs;
        popupPanel.SetActive(true);
    }

    public IEnumerator ShowTradeProposal()  // Proposal Window + Window where Players can decline & accept trades
    {
        yield return wfs;
        tradeProposalVersion.SetActive(true);
        tradeConfirmationVersion.SetActive(false);
        tradeProposal.SetActive(true);
    }

    public IEnumerator ShowTradeConfirmation()  // Proposal Window + Window where playerAtTrun can react to other Players willingness to trade
    {
        yield return wfs;
        tradeProposalVersion.SetActive(false);
        tradeConfirmationVersion.SetActive(true);
        tradeProposal.SetActive(true);

        bool accepted = false;
        ProposalManager proposalManager = pm.GetComponent<ProposalManager>();
        foreach (Player p in board.players)
            if (p != board.playerAtTurn && proposalManager.playerList[p])
                accepted = true;

        if (accepted) yield break;
        Confirm cb = confirmButton.GetComponent<Confirm>();
        cb.GetComponent<Confirm>().CurrentSelectable = proposalManager;
        StartCoroutine(cb.ShowConfirmButton());
    }

    private void Update()  // Checks if player wants to deselect
    {
        if ((board.phase == Phase.TRADING && board.playerAtTurn.proposedTradeGive != null || board.phase == Phase.BUILDING || board.phase == Phase.CHOOSEVICTIM) && Input.GetKeyDown(KeyCode.Escape))
        {
            confirmButton.SetActive(false);
            confirmButton.GetComponent<Confirm>().CurrentSelectable = null;
            board.playerAtTurn.HidePopupInstructions();
        }
    }
}
