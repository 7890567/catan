using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum StreetType { Land, Water }

[ExecuteInEditMode]
public class Street : SelectableObject
{
    public StreetType type = StreetType.Land;
    //[HideInInspector] [SerializeReference] public Player player; //{ get; private set; } = null;
    public bool built { get; private set; } = false;
    // In this array [1] is always the intersection with the highest z coordinate
    public Intersection[] intersections = new Intersection[2];

    private GameObject _structure;

    public override void Confirmed()
    {
        Player clickingPlayer = board.playerAtTurn;
        if (board.phase != Phase.INITIALPLACEMENT) clickingPlayer.HidePopupInstructions();
        if (clickingPlayer.initialRoad && !board.inUI && StreetCanBeBuilt(clickingPlayer, false, false))
        {
            BuildStreet(clickingPlayer);
            clickingPlayer.initialRoad = false;
            clickingPlayer.HidePopupInstructions();
            //clickingPlayer.BuildRoadText(false);
            board.NextInitial();
        }

        if (!board.inUI && !built && (board.phase == Phase.BUILDING || board.phase == Phase.FREEROADS) && StreetCanBeBuilt(clickingPlayer, false, false))
            BuyStreet(clickingPlayer);
    }

    public override bool BuildingCanBeBuilt()
    {
        return StreetCanBeBuilt(board.playerAtTurn, false, false);
    }

    public bool StreetCanBeBuilt(Player p, bool checkResources, bool checkRoadsLeft)
    {
        if (built || (checkRoadsLeft && p.roadsLeft == 0) || (checkResources && !p.HasResources(ResourceSet.streetCost)) || (p.initialRoad && intersections[0].player != p && intersections[1].player != p))
            return false;
        
        for (int i = 0; i < 2; i++)
        {
            if (p.initialRoad && intersections[i].player == p && ((intersections[i].streets[0]?.built ?? false) || (intersections[i].streets[1]?.built ?? false) || (intersections[i].streets[2]?.built ?? false)))
                return false;
            if (intersections[i].player == p)
                return true;
            for (int j = 0; j < 3; j++)
            {
                if (intersections[i].streets[j] == this)
                    continue;
                if (intersections[i].streets[j]?.player == p)
                    return true;
            }
        }
        return false;
    }
    
    public void BuyStreet(Player p)
    {
        if (p.roadsLeft == 0)
        {
            p.Popup("You don't have any streets left to build.", "Building");
            return;
        }
        if (board.phase == Phase.FREEROADS)
        {
            BuildStreet(p);
            p.freeRoads--;
            if (p.freeRoads == 0)
            {
                board.playerAtTurn.HidePopupInstructions();
                board.phase = Phase.BUILDING;
            }              
            else
                board.playerAtTurn.ShowPopupInstructions("1 Road left");
        }
        else
        {
            if (p.HasResources(ResourceSet.streetCost))
            {
                p.AddResources(ResourceSet.streetCost.Negative);
                BuildStreet(p);
            }
            else
                p.NotEnoughResources();
        }
    }

    public Intersection OtherIntersection(Intersection i)
    {
        return intersections[intersections[0] == i ? 1 : 0];
    }

    public void BuildStreet(Player p)
    {
        player = p;
        built = true;
        p.roadsLeft--;
        
        if (Board.PlayPhysical)
        {
            if (type == StreetType.Land)
                _structure = Instantiate(board.BuildingPrefabs[0], transform);
            else
                throw new NotImplementedException();
            _structure.GetComponent<MeshRenderer>().SetPropertyBlock(player.mpb);

            StartCoroutine(board.Rehighlight());

            p.UpdateStructuresUI();
        }

        board.LongestRoad(p);
    }
}
