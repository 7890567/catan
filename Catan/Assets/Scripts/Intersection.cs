using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

public enum BuildingType { Empty, Settlement, City }
public enum HarborType { None, Three, Wood, Stone , Wool, Wheat, Ore }  // These are in the same order as TileType.

[ExecuteInEditMode]
public class Intersection : SelectableObject
{
    [SerializeField] private List<TileType> neededResources2;

    public BuildingType building = BuildingType.Empty;
    public bool Built => building != BuildingType.Empty;
    //[HideInInspector] [SerializeReference] public Player player = null;
    public HarborType harbor = HarborType.None;
    // 0: top left street 1: top right street 2: bottom middle street
    // or (depending on position of intersection)
    // 0: top middle street 1: bottom right 2: bottom left
    public Street[] streets = new Street[3];
    public Tile[] surroundingTiles = new Tile[3];

    private GameObject _structure;
    
    public override void Confirmed()
    {
        Player clickingPlayer = board.playerAtTurn;
        if (board.phase != Phase.INITIALPLACEMENT) clickingPlayer.HidePopupInstructions();
        if (board.phase == Phase.CHOOSEVICTIM && player != board.playerAtTurn && player != null)
        {
            clickingPlayer.HidePopupInstructions();
            foreach (Intersection i in board.robberTile.surroundingIntersections)
                if (i == this)
                    board.StealCard(player);
            return;
        }
        if (clickingPlayer.initialSettlement && SettlementCanBeBuilt(clickingPlayer, false, false) && !board.inUI)
        {
            clickingPlayer.HidePopupInstructions();
            BuildSettlement(clickingPlayer);
            clickingPlayer.initialSettlement = false;
            clickingPlayer.initialRoad = true;
            clickingPlayer.ShowPopupInstructions("Build a Road");
            if (Board.PlayPhysical)
                StartCoroutine(board.Rehighlight());
        }

        if (board.inUI || board.phase != Phase.BUILDING) return;
        switch (building)
        {
            case BuildingType.Empty when SettlementCanBeBuilt(clickingPlayer, false, false) && !board.inUI:
                BuySettlement(clickingPlayer);
                break;
            case BuildingType.Settlement when player.AtTurn && !board.inUI:
                BuyCity(clickingPlayer);
                break;
        }
    }

    public void BuySettlement(Player p)
    {
        if (p.settlementsLeft == 0)
        {
            p.Popup("You don't have any settlements left to build.", "Development");
            return;
        }

        if (!p.HasResources(ResourceSet.settlementCost))
        {
            p.NotEnoughResources();
            return;
        }
        
        p.AddResources(ResourceSet.settlementCost.Negative);
        BuildSettlement(p);
    }

    public override bool BuildingCanBeBuilt()
    {
        if (building == BuildingType.Empty)
        {
            return SettlementCanBeBuilt(board.playerAtTurn, false, false);
        }
        return building == BuildingType.Settlement && (board.phase == Phase.CHOOSEVICTIM || board.phase == Phase.BUILDING && player == board.playerAtTurn);
    }

    public bool SettlementCanBeBuilt(Player p, bool checkResources, bool checkSettlementsLeft)
    {
        if ((checkSettlementsLeft && p.settlementsLeft == 0) || (checkResources && !p.HasResources(ResourceSet.settlementCost)))
            return false;
        
        bool connectingStreet = p.initialSettlement;
        bool adjacentBuilding = false;
        bool alreadyBuilding = false;
        for (int i = 0; i < 3; i++)
        {
            if(streets[i] == null) continue;
            if (streets[i].player == p) connectingStreet = true;
            if (streets[i].OtherIntersection(this)?.building != BuildingType.Empty) adjacentBuilding = true;
            if (building != BuildingType.Empty) alreadyBuilding = true;
        }

        return connectingStreet && !adjacentBuilding && !alreadyBuilding;
    }

    public bool AdjacentSettlements(Player p)
    {

        bool adjacentBuilding = false;
        bool alreadyBuilding = false;
        for (int i = 0; i < 3; i++)
        {
            if (streets[i] == null) continue;
            if (streets[i].OtherIntersection(this)?.building != BuildingType.Empty) adjacentBuilding = true;
            if (building != BuildingType.Empty) alreadyBuilding = true;
        }

        return !adjacentBuilding && !alreadyBuilding;
    }
    
    public void BuyCity(Player p)
    {
        if (p.citiesLeft == 0)
        {
            p.Popup("You don't have any cities left to build.", "Development");
            return;
        }
        if (p.HasResources(ResourceSet.cityCost))
        {
            p.AddResources(ResourceSet.cityCost.Negative);
            UpgradeToCity();
        }
        else
            player.NotEnoughResources();
    }

    public bool CityCanBeBuilt(Player p)  // This method is only used by the highlighting in Board.cs and by the AI
    {
        return building == BuildingType.Settlement && p == player && p.citiesLeft > 0 && p.HasResources(ResourceSet.cityCost);
    }
    
    public void GiveResources(TileType resource)
    {
        if (player == null) return;
        
        switch (building)
        {
            case BuildingType.Settlement:
                player.resources[resource]++;
                break;
            case BuildingType.City:
                player.resources[resource] += 2;
                break;
        }
        board.playerAtTurn.UpdateResourcesUI();
    }
    
    public void BuildSettlement(Player p)
    {
        player = p;
        if (harbor != HarborType.None)
        {
            p.harbors.Add(harbor);
            p.UpdateHarborsUI();
        }

        building = BuildingType.Settlement;
        foreach (TileType i in Enum.GetValues(typeof(TileType)))
            resourceCost[i] = 0;
        foreach (TileType i in neededResources2)
            resourceCost[i]++;


        p.settlementsLeft--;
        p.AddVictoryPoints(1);

        if (Board.PlayPhysical)
        {
            _structure = Instantiate(board.BuildingPrefabs[1], transform.position, Quaternion.Euler(0, Random.value * 360, 0), transform);
            _structure.GetComponent<MeshRenderer>().SetPropertyBlock(p.mpb);
            
            StartCoroutine(board.Rehighlight());
            
            p.UpdateStructuresUI();
            p.UpdatePlayerUI();
        }
        if (player.initialSettlement)
            foreach (Tile t in board.tiles.Where(t => t.surroundingIntersections.Contains(this) && t.number != 0))
                GiveResources(t.type);
    }

    private void UpgradeToCity()
    {
        building = BuildingType.City;

        player.citiesLeft--;
        player.settlementsLeft++;
        player.AddVictoryPoints(1);

        if (Board.PlayPhysical)
        {
            Destroy(_structure.gameObject);
            _structure = Instantiate(board.BuildingPrefabs[2], transform.position, Quaternion.Euler(0, Random.value * 360, 0), transform);
            _structure.GetComponent<MeshRenderer>().SetPropertyBlock(player.mpb);

            StartCoroutine(board.Rehighlight());

            player.UpdateStructuresUI();
            player.UpdatePlayerUI();
        }
    }
    
    public int LongestRoad(List<string> alreadyChecked, Player p)
    {
        int[] results = new int[3];
        for (int i = 0; i < 3; i++)
        {
            // First make a copy of the the list with already checked streets, otherwise we would be modifying the same list a lot of other function are using
            List<string> listCopy = new List<string>(alreadyChecked);
            if (streets[i]?.player == p && !alreadyChecked.Contains(streets[i]?.name))
            {
                listCopy.Add(streets[i].name);
                Intersection otherIntersection = streets[i].OtherIntersection(this);
                if (otherIntersection.building != BuildingType.Empty && otherIntersection.player != p)   // If a building of a enemy player is blocking the longest road
                    results[i] = 1;
                else
                    results[i] = 1 + streets[i].OtherIntersection(this).LongestRoad(listCopy, p);
            }
            else
            {
                results[i] = 0;
            }
        }
        return results.Max();
    }
}
