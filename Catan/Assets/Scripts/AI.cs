﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Random = UnityEngine.Random;
using UnityEngine.SceneManagement;

public abstract class AI : Player
{
    protected int stackOverflowPreventer;

    protected AI(Board b, UIHandler h, Color c, string s) : base(b, h, c, s)
    {
        
    }
    
    public override void Popup(string message, string phase)
    {
        if(board.phase == Phase.INITIALPLACEMENT)
            InitialPlacement();
    }
    
    public override void ThrowDice()
    {
        board.GetDiceSides(Random.Range(1,7),Random.Range(1,7));
    }

    public void RespondToOffer(bool acceptTrade)  // Making the decision to accept/decline the trade
    {
        if (acceptTrade)
            ui.pm.GetComponent<ProposalManager>().playerList[this] = true;
        board.ProposePlayerTrade();
    }
    
    public override void ProposeFinalTrade()
    {
        List<Player> playersAccepted = new List<Player>();
        foreach(Player p in board.players)
        {
            if (p == this)
                continue;
            if(ui.pm.GetComponent<ProposalManager>().playerList[p])
                playersAccepted.Add(p);
        }
        AcceptedTrade(playersAccepted);
    }
    
    public override void StartTurn()
    {
        if (board.playerAtTurn != this || board.gameEnded)  // Just a failsafe for possible bugs, though they might not be necessary
            return;
        stackOverflowPreventer = 0;
        ThrowDice();
    }

    public override void DismissPopup()  // Required for other player's doing actions in AI's turn (such as discarding)
    {
        if (board.gameEnded)
        {
            Scene scene = SceneManager.GetActiveScene(); SceneManager.LoadScene(scene.name);
        }
        ui.popupPanel.SetActive(false);
        if (popupUI) board.inUI = false;
        popupUI = false;
        if (board.phase == Phase.TRADING && !Board.CheckEmpty() && !ui.tradeProposal.activeInHierarchy && board.playerTradingNumber > -1) ShowProposeTrade();
    }

    protected override void WinGame(bool victoryCardsUsed)
    {
        board.gameEnded = true;
        if (victoryCardsUsed)
            base.Popup(playerName + " has got enough victory points!\n" + playerName + " wins the game!", "Victory");
        else
            base.Popup(playerName + " has got enough victory cards to get 10 points!\n" + playerName + " wins the game!", "Victory");
        base.NextPlayerUI();  // AI does not usually update UI, but the game stops here in an AI's turn, so it would be nice to see the UI up to date.
        for (int i = 0; i < 100; i++) (this as Player).ThrowDice();
    }

    // This function mostly might be usefull as a negative reward for reinforcement learning
    public abstract override void NotEnoughResources();
    public abstract override void ChooseRobberVictim(Tile tile);
    public abstract override void ProposeTrade();
    public abstract void InitialPlacement();
    public abstract void AcceptedTrade(List<Player> acceptedPlayers);
    
    /*---------------------------------------------
     below are ui functions that are no use to the ai 
     overridden with empty functions
     ----------------------------------------------*/
    public override void UpdateHarborsUI() {}
    public override void UpdateStructuresUI() {}
    public override void UpdatePlayerUI() {}
    public override void UpdatePhaseUI() {}
    public override void UpdateDevCards() {}
    public override void ResetView() {}
    public override void NextPlayerUI() {}
    public override void UpdateResourcesUI() {}
    public override void ShowStolenCard(TileType resource) {}
    public override void Panel(int panelNumber) {}
    public override void DevActionPanel(int panelNumber) {}

}
