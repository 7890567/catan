using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(Street))]
public class StreetEditor : Editor
{
    private SerializedObject obj;

    public void OnEnable()
    {
        obj = new SerializedObject(target);
    }

    public override void OnInspectorGUI()
    {
        obj.Update();
        Street streetScript = (Street)target;

        DrawDefaultInspector();
        
        EditorGUILayout.LabelField("Player: " + (streetScript.player?.playerName ?? "None"));

        obj.ApplyModifiedProperties();
    }
}