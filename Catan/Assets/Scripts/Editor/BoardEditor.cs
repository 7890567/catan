using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;
using UnityEngine.Tilemaps;

[CustomEditor(typeof(Board))]
public class BoardEditor : Editor
{
    private SerializedObject obj;
    private SerializedProperty tiles, intersectionPrefab, intersections, streetPrefab, streets;
    private SerializedProperty players;

    private bool _showTilePrefabs = true, _showHarborPrefabs = true, _showBuildingPrefabs = true, _showHarbors = true;

    public void OnEnable()
    {
        obj = new SerializedObject(target);
        tiles = obj.FindProperty("tiles");
        intersectionPrefab = obj.FindProperty("intersectionPrefab");
        streetPrefab = obj.FindProperty("streetPrefab");
        intersections = obj.FindProperty("intersections");
        streets = obj.FindProperty("streets");

        players = obj.FindProperty("players");
    }

    public override void OnInspectorGUI()
    {
        obj.Update();
        Board boardScript = (Board)target;

        if (GUILayout.Button("Rewake"))
            boardScript.Awake();

        _showTilePrefabs = EditorGUILayout.Foldout(_showTilePrefabs, "Tile Prefabs", true);
        if (_showTilePrefabs)
        {
            EditorGUI.indentLevel++;
            foreach (TileType type in (TileType[])Enum.GetValues(typeof(TileType)))
            {
                //Board.TilePrefabs[type] = (GameObject)EditorGUILayout.ObjectField(type.ToString(), Board.TilePrefabs[type], typeof(GameObject), false);
                boardScript.TilePrefabs[(int)type] = (GameObject)EditorGUILayout.ObjectField(type.ToString(),
                    boardScript.TilePrefabs[(int)type], typeof(GameObject), false);
            }
            EditorGUI.indentLevel--;
        }
        
        _showHarborPrefabs = EditorGUILayout.Foldout(_showHarborPrefabs, "Harbor Prefabs", true);
        if (_showHarborPrefabs)
        {
            EditorGUI.indentLevel++;
            foreach (HarborType type in ((HarborType[])Enum.GetValues(typeof(HarborType))).Skip(1))
            {
                boardScript.HarborPrefabs[(int)type - 1] = (GameObject)EditorGUILayout.ObjectField(type.ToString(),
                    boardScript.HarborPrefabs[(int)type - 1], typeof(GameObject), false);
            }
            EditorGUI.indentLevel--;
        }

        _showBuildingPrefabs = EditorGUILayout.Foldout(_showBuildingPrefabs, "Building Prefabs", true);
        if (_showBuildingPrefabs)
        {
            EditorGUI.indentLevel++;
            boardScript.BuildingPrefabs[0] = (GameObject)EditorGUILayout.ObjectField("Land Street",
                boardScript.BuildingPrefabs[0], typeof(GameObject), false);
            boardScript.BuildingPrefabs[1] = (GameObject)EditorGUILayout.ObjectField("Settlement",
                boardScript.BuildingPrefabs[1], typeof(GameObject), false);
            boardScript.BuildingPrefabs[2] = (GameObject)EditorGUILayout.ObjectField("City",
                boardScript.BuildingPrefabs[2], typeof(GameObject), false);
            EditorGUI.indentLevel--;
        }
        
        EditorGUILayout.PropertyField(tiles);
        
        if (GUILayout.Button("Fix Tile List"))
            boardScript.GetAllTiles();
        
        if (GUILayout.Button("Randomize Resources"))
            boardScript.RandomizeTileResources();
        
        if (GUILayout.Button("Assign Tile Numbers"))
            boardScript.AssignTileNumbers(boardScript.getTileByCoordinates(new Vector3Int(-1 ,2,0)));

        EditorGUILayout.PropertyField(intersectionPrefab);
        EditorGUILayout.PropertyField(streetPrefab);
        EditorGUILayout.PropertyField(intersections);
        EditorGUILayout.PropertyField(streets);

        if (GUILayout.Button("Fix Intersections and Street Lists")) {
            boardScript.GetAllIntersections();
            boardScript.GetAllStreets(); }

        if (GUILayout.Button("Remake Intersections and Streets"))
            boardScript.MakeAllIntersections();
        
        GUILayout.BeginHorizontal();
        _showHarbors = EditorGUILayout.Foldout(_showHarbors, "Harbors", true);
        if (GUILayout.Button("Remove All"))
        {
            boardScript.harborLocations.Clear();
            boardScript.harborConnections.Clear();
            boardScript.harborTypes.Clear();
        }
        GUILayout.EndHorizontal();
        if (_showHarbors)
        {
            EditorGUI.indentLevel++;
            for (int i = 0; i < boardScript.harborLocations.Count; i++)
            {
                Tile tile = boardScript.harborLocations[i];
                tile = (Tile)EditorGUILayout.ObjectField("Harbor " + (i+1), tile, typeof(Tile), true);
                
                if (tile?.type != TileType.Water || (boardScript.harborLocations[i] != tile && boardScript.harborLocations.Contains(tile)))
                {
                    boardScript.harborLocations.RemoveAt(i);
                    boardScript.harborConnections.RemoveAt(i);
                    boardScript.harborTypes.RemoveAt(i);
                    continue;
                }
                boardScript.harborLocations[i] = tile;
                
                /*if (tile.IntersectionCount == 2)
                { 
                    for (int j = 0; j < 6; j ++)
                        if (tile.surroundingIntersections[j] != null && tile.surroundingIntersections[Tile.ClockwiseNextIndex(j)] != null)
                        {
                            boardScript.harborConnections[i] = j;
                            break;
                        }
                    continue;
                }*/

                /*EditorGUI.indentLevel++; GUILayout.BeginHorizontal();
                foreach (int j in Enumerable.Range(0, 6).Where(index =>
                    tile.surroundingIntersections[index] != null &&
                    tile.surroundingIntersections[Tile.ClockwiseNextIndex(index)] != null))
                {
                    EditorGUILayout.ToggleLeft(j.ToString(), true, (GUIStyle)"Radio", GUILayout.Width(55));
                }
                GUILayout.EndHorizontal(); EditorGUI.indentLevel --;*/
                string[] indexOptions = (from index in Enumerable.Range(0, 6)
                    where tile.surroundingIntersections[index] != null &&
                          tile.surroundingIntersections[Tile.ClockwiseNextIndex(index)] != null
                    select index.ToString()).ToArray();
                EditorGUI.indentLevel++; GUILayout.BeginHorizontal(); GUILayout.Space(30);
                boardScript.harborConnections[i] = int.Parse(indexOptions[
                    GUILayout.SelectionGrid(
                        indexOptions.Contains(boardScript.harborConnections[i].ToString())
                            ? Array.IndexOf(indexOptions, boardScript.harborConnections[i].ToString())
                            : 0, indexOptions, 6)]);
                HarborType harborType = boardScript.harborTypes[i];
                harborType = (HarborType)EditorGUILayout.EnumPopup(harborType);
                //if (GUILayout.Button("Remove"))
                if (harborType == HarborType.None)
                {
                    boardScript.harborLocations.RemoveAt(i);
                    boardScript.harborConnections.RemoveAt(i);
                    boardScript.harborTypes.RemoveAt(i);
                    continue;
                }
                boardScript.harborTypes[i] = harborType;
                GUILayout.EndHorizontal(); EditorGUI.indentLevel--;
            }  // End of foreach tile
            
            Tile newTile = (Tile)EditorGUILayout.ObjectField("New Harbor", null, typeof(Tile), true);
            if (newTile?.type == TileType.Water && !boardScript.harborLocations.Contains(newTile))
            {
                boardScript.harborLocations.Add(newTile);
                boardScript.harborConnections.Add(-1);
                boardScript.harborTypes.Add(HarborType.Three);
            }
            
            /*GUILayout.BeginHorizontal(); GUILayout.Space(15); if (GUILayout.Button("Add"))
            {
                boardScript.harborLocations.Add(null);
                boardScript.harborConnections.Add(-1);
            } GUILayout.EndHorizontal();*/

            EditorGUI.indentLevel--;
        }  // End of harbors

        if (GUILayout.Button("Remake Harbors"))
            boardScript.MakeAllHarbors();

        EditorGUILayout.PropertyField(players);

        obj.ApplyModifiedProperties();
    }
}
