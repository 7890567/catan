using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(Intersection))]
public class IntersectionEditor : Editor
{
    private SerializedObject obj;

    public void OnEnable()
    {
        obj = new SerializedObject(target);
    }

    public override void OnInspectorGUI()
    {
        obj.Update();
        Intersection intersectionScript = (Intersection)target;

        DrawDefaultInspector();
        
        EditorGUILayout.LabelField("Player: " + (intersectionScript.player?.playerName ?? "None"));
        
        obj.ApplyModifiedProperties();
    }
}