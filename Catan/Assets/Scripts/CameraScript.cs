using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraScript : MonoBehaviour
{
    [SerializeField] private Transform focalPoint;
    private Vector3 _lastMousePosition;
    public float rotationSpeed;
    [Min(0)] public float minXAngle = 0;
    [Range(0, 90)] public float maxXAngle = 90;

    public Board board;
    private GameObject menUI;

    private void Awake()
    {
        focalPoint = transform.parent;
        menUI = GameObject.Find("Menu");
    }

    private void Update()
    {
        //Checks if camera can be rotated
        if ((board.diceThrown || board.phase == Phase.INITIALPLACEMENT) && Input.GetMouseButton(1) && !menUI.activeInHierarchy)
        {
            Vector3 mouseDelta = Input.mousePosition - _lastMousePosition;
            transform.RotateAround(focalPoint.position, Vector3.up, mouseDelta.x * rotationSpeed * Time.deltaTime);
            transform.RotateAround(focalPoint.position, transform.right, Mathf.Clamp(-mouseDelta.y * rotationSpeed * Time.deltaTime, minXAngle - transform.rotation.eulerAngles.x, maxXAngle - transform.rotation.eulerAngles.x));
        }
        _lastMousePosition = Input.mousePosition;
    }
}
