using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEditor;
using UnityEngine;
using Debug = UnityEngine.Debug;
using Random = UnityEngine.Random;
using System.Linq;
using UnityEngine.UI;
using TMPro;

public enum Phase { DICE, TRADING, BUILDING, NEXTTURN, FREEROADS, INITIALPLACEMENT, ROBBER, CHOOSEVICTIM }

[ExecuteInEditMode]
public class Board : MonoBehaviour
{
    public static readonly bool PlayPhysical = true;
    
    public Phase phase;
    public Phase returnPhase;
    public bool diceThrown, gameEnded;
    public bool inUI, turnBlock;

    public Player[] players;

    public int playerTurnNumber;
    public int actualTurn;  // Only useful for initial phases
    [SerializeReference] public Player playerAtTurn;
    [SerializeReference] public Player playerMostKnights;
    [SerializeReference] public Player playerLongestRoad;
    public int longestRoadLength = 0;

    public Tile robberTile;  // Saves the tile the robber is currently on, so that not all tiles have to be searched through to find it again upon setting hasRobber to false

    private bool testGame = false;  // True means, we skip the initial building phase, and the first 'next' click starts the first turn
    
    public GameObject[] TilePrefabs = new GameObject[7];  // In the order of the TileType enum
    public GameObject[] HarborPrefabs = new GameObject[6];  // In the order of the HarborType enum, excluding None
    public GameObject[] BuildingPrefabs = new GameObject[3];  // Order: Street, Settlement, City

    public GameObject intersectionPrefab;
    public GameObject streetPrefab;

    public List<Tile> tiles;
    public List<Intersection> intersections;
    public List<Street> streets;
    public List<Tile> harborLocations; public List<int> harborConnections; public List<HarborType> harborTypes;
    
    public UIHandler ui;
    private Transform robber;
    public Plenty plenty;

    public int playerTradingNumber;

    private int playersThatDiscarded;

    public void Awake()
    {
        ui.menu.SetActive(true);

        // To allow the Awake() of the attached script to get a reference the Confirm Button
        ui.confirmButton.SetActive(true);
        ui.panels[1].SetActive(true);
        ui.panels[2].SetActive(true);
        ui.devActionPanels[0].SetActive(true);
        ui.devActionPanels[1].SetActive(true);
        ui.pm.SetActive(true);
        ui.pm.SetActive(false);
        ui.tradeProposal.SetActive(true);
        ui.tradeProposal.SetActive(false);

        plenty = GameObject.Find("Panel - DevCard - Year Of Plenty").GetComponent<Plenty>();
        foreach (GameObject go in ui.panels)
            go.SetActive(false);  // To prevent dice and trading from appearing in reverse if someone edited them
        ui.devActionPanels[0].SetActive(false);
        ui.devActionPanels[1].SetActive(false);
        ui.popupPanel.SetActive(false);
        inUI = true;

        ui = gameObject.GetComponent<UIHandler>();
        robber = GameObject.Find("Robber").transform;

        Tile.TileCreated -= AddTile;
        Tile.TileDestroyed -= RemoveTile;
        Tile.TileCreated += AddTile;
        Tile.TileDestroyed += RemoveTile;

        robberTile = GameObject.Find("Empty").GetComponent<Tile>();

        FillDevCardStack();
        foreach (DevelopmentCard dc in devCardStack)
            dc.board = this;


        // 'Artificial' turns
        playerTurnNumber = -1;
        playerTradingNumber = -1;
    }

    private void OnValidate()
    {
        foreach (Player p in players) p.OnValidate();
    }

    public void StartGame()  // Called from Start Menu
    {
        int aiAmount = 0; //to prevent a game with 4 ai
        for (int i = 0; i < 4; i++)
        {
            if (ui.AI[i].isOn)
            {
                players[i] = new RuleBasedAi(this, ui, ui.colorPickers[i].color, ui.nameImputs[i].text);
                aiAmount++;
            }
            else players[i] = new Player(this, ui, ui.colorPickers[i].color, ui.nameImputs[i].text);
        }
        playerAtTurn = players[0];

        if(aiAmount > 3)
        {
            new Player(this, ui, Color.red, "placeholder player").Popup("You cannot play with 4 AI, please select at least one player", "Start Menu");
            return;
        }
        
        foreach (TMP_InputField nameField in ui.nameImputs.Where(nameField => nameField.text.Length > 12))
        {
            playerAtTurn.Popup("One of the names is too long: " + nameField.text , "Start Menu");
            return;
        }
        foreach (TMP_InputField nameField in ui.nameImputs.Where(nameField => nameField.text == ""))  // Second loop, so that it doesn't fill in part of the empty names if the game doesn't start
        {
            // playerAtTurn.Popup("One of the name fields is empty");
            nameField.text = "Nobody";
        }
        
        ui.pm.GetComponent<ProposalManager>().ResetPanel();

        // Check if the colors & names don't correspond with each other and if the colors aren't too dark (since all the UI text is black)
        Color tempColor = ui.colorPickers[0].color;
        string tempName = ui.nameImputs[0].text;
        for (int i = 1; i < 4; i++)
        {
            if (ui.colorPickers[i].color == tempColor)
            {
                playerAtTurn.Popup("At least 1 pair of chosen colors corresponds with one another, choose another color", "Start Menu");
                return;
            }
            if (ui.nameImputs[i].text == tempName)
            {
                playerAtTurn.Popup("At least 1 pair of chosen names corresponds with one another, chosse another name", "Start Menu");
                return;
            }
            tempColor = ui.colorPickers[i].color;
            tempName = ui.nameImputs[i].text;
        }
        for (int i = 0; i < 4; i++)
        {
            if (ui.colorPickers[i].color.grayscale < 0.114f)
            {
                playerAtTurn.Popup(ui.nameImputs[i].text + ", the color you have chosen is too dark", "Start Menu");
                return;
            }
        }

        if (ui.randomResources.isOn)  // If toggle selected in Menu to randomize Resource for each Tile
            RandomizeTileResources();

        testGame = ui.testGame.isOn;

        for (int i = 0; i < players.Length; i++)
        {
            players[i].playerName = ui.nameImputs[i].text;
            players[i].color = ui.colorPickers[i].color;
            players[i].OnValidate();
            
            ui.playerNames[i].text = players[i].playerName;
            ui.playerColorPanels[i].GetComponent<Image>().color = ui.colorPickers[i].color;
        }

        ui.menu.SetActive(false);
        inUI = false;

        if (testGame)
        {
            phase = Phase.BUILDING;  // That is turned into Dice at the first Next
            Next();
        }
        else
        {
            phase = Phase.INITIALPLACEMENT;
            NextInitial();
            if (PlayPhysical) 
                StartCoroutine(Rehighlight());
        }
    }

    public void Next()  // Next phase
    {
        /*ui.phases[2].SetActive(false);
        ui.confirmButton.SetActive(false);
        ui.confirmButton.GetComponent<Confirm>().CurrentSelectable = null;*/

        if (gameEnded)
            return;
        phase++;
        if (phase == Phase.ROBBER || phase == Phase.CHOOSEVICTIM)
            phase = Phase.NEXTTURN;
        switch (phase)
        {
            case Phase.NEXTTURN:
                playerTurnNumber++;
                if (playerTurnNumber >= players.Length) playerTurnNumber = 0;
                playerAtTurn = players[playerTurnNumber];
                devCardPlayed = false;
                knightPlayed = false;
                playerAtTurn.ResetView();
                playerAtTurn.ResetSmallDice();
                playerAtTurn.HidePopupInstructions();
                phase = Phase.DICE;
                playerAtTurn.StartTurn();
                break;
            case Phase.TRADING:
                playerAtTurn.Trading();
                break;
            case Phase.BUILDING:
                playerAtTurn.Development();
                playerAtTurn.EndTurn();
                break;
        }
        if (PlayPhysical)
            StartCoroutine(Rehighlight());
        playerAtTurn.UpdatePhaseUI();
        playerAtTurn.ResetOpenIcon();
    }

    public void NextInitial()  // Next 'phase' when initial phase
    {
        if (playerTurnNumber >= 7)
        {
            Next();
            return;
        }
        playerTurnNumber++;
        if (playerTurnNumber > 3)
            actualTurn = 7 - playerTurnNumber;
        else actualTurn = playerTurnNumber;
        playerAtTurn = players[actualTurn];
        playerAtTurn.initialSettlement = true;
        playerAtTurn.Popup(playerAtTurn.playerName + " is at turn now.", "Initial Placement");
        playerAtTurn.NextPlayerUI();
    }

    public void GetDiceSides(int die1, int die2)  // When dice are rolled and numbers are confirmed
    {
        if (gameEnded)
            return;
        int Number = die1 + die2;
        
        if (Number == 7)
        {
            returnPhase = Phase.DICE;
            RobberPhase();
            playerDiscardCounter = -1;  // Because the method starts with an increase
            CheckForDiscard();
        }
        else
        {
            foreach (Tile tile in tiles.Where(tile => tile.number == Number && !tile.hasRobber))
                tile.NumberRolled();
            Next();
        }
    }

    /*--------------------------------------------------------------------*/
    /*-----------------------------[ Robber ]-----------------------------*/
    /*--------------------------------------------------------------------*/
    
    public int playerDiscardCounter = 0;  // =0 To prevent null references  // The default value of an int is 0, not null, so =0 is unnecessary
    public Player discardingPlayer => players[playerDiscardCounter];
    public void RobberPhase()
    {
        phase = Phase.ROBBER;
        StartCoroutine(Rehighlight());
    }

    private void CheckForDiscard()
    {
        while (true)
        {
            playerDiscardCounter++;
            if (playerDiscardCounter >= players.Length)  // This is where all the players have been checked and have discarded if required. This is where it truly switches to robber.
            {
                if (playersThatDiscarded > 0)
                {
                    playerAtTurn.ShowExpandedResourcesUI(false);
                    playerAtTurn.ShowExpandableResourcesUI(true);
                    playerAtTurn.ResetOpenIcon();
                    playersThatDiscarded = 0;
                }

                playerAtTurn.ShowPopupInstructions("Choose a Tile to target");
                plenty.takeCards = false;
                playerDiscardCounter = 0;  // For null references again
                playerAtTurn.Robber();  // For AI
                return;
            }

            int resAmount = discardingPlayer.ResourceAmount;
            if (resAmount <= 7) continue;
            Plenty(true, resAmount / 2);
            playersThatDiscarded++;
            break;
        }
    }

    public void MoveRobber(Tile targetTile)
    {
        //Robber movement in data
        robberTile.hasRobber = false;
        robberTile = targetTile;
        robberTile.hasRobber = true;

        if (PlayPhysical)
            robber.position = robberTile.transform.position + new Vector3(0, 3.692f, 0);

        playerAtTurn.ChooseRobberVictim(robberTile);
    }

    public void StealCard(Player target)
    {
        // Makes list of all resources the target has
        List<TileType> weightedList = new List<TileType>();
        foreach (TileType tileType in Enum.GetValues(typeof(TileType)))
            if (tileType != TileType.Empty && tileType != TileType.Water)
                weightedList.AddRange(Enumerable.Repeat(tileType, target.resources[tileType]).ToList());

        // Chooses a random resource from the list
        if (target.ResourceAmount != 0)
        {
            TileType resource = weightedList[UnityEngine.Random.Range(0, target.ResourceAmount)];
            target.resources[resource]--;
            playerAtTurn.resources[resource]++;
            playerAtTurn.ShowStolenCard(resource);
            playerAtTurn.UpdateResourcesUI();
            ReturnFromRobber();
        }
        else playerAtTurn.Popup("That player hasn't got any resources to steal!", "Robber");
    }

    public void ReturnFromRobber()
    {
        phase = returnPhase;
        if (phase == Phase.DICE)
            Next();
        else StartCoroutine(Rehighlight());
    }

    /*------------------------------------------------------------------------*/
    /*------------------[ Special Cards for Victory Points ]------------------*/
    /*------------------------------------------------------------------------*/

    public void UpdateMostKnights()
    {
        Player newMostKnights = HasMostKnights();
        if (newMostKnights != playerMostKnights)
        {
            if (playerMostKnights != null)
                playerMostKnights.AddVictoryPoints(-2);
            playerMostKnights = newMostKnights;
            playerMostKnights.AddVictoryPoints(2);
            if (gameEnded) return;
            playerAtTurn.Popup(playerAtTurn.playerName + " has the largest army.", "Largest Army");
        }
        playerAtTurn.UpdatePlayerUI();
    }

    private Player HasMostKnights()  // Check which player has the most knights
    {
        // These will save the player with the most knights while counting
        Player newMostKnights = null;  // The method does not give errors if no new player is assigned for the return
        int mostKnights = 2;  // Only if a player has more than 2 knights will they get the Largest Army

        foreach (Player p in players)
            if (p.knightsUsed > mostKnights)
            {
                mostKnights = p.knightsUsed;
                newMostKnights = p;
            }
        return newMostKnights;
    }

    public int LongestRoad(Player p)  // Checks which player has the longest road
    {
        List<int> results = new List<int>() {0};
        
        // Call longest road function on every intersection, and add the results to a list
        results.AddRange(intersections.Select(intersection => intersection.LongestRoad(new List<string>(), p)));
        
        int res = results.Max();

        if (res > p.roadLength)  // for UI purposes
            p.roadLength = res;
        
        if (res >= 5)
        {
            if (res > longestRoadLength)
                longestRoadLength = res;
            if (playerLongestRoad == null)
            {
                playerLongestRoad = p;
                p.AddVictoryPoints(2);
                longestRoadLength = res;
                if (gameEnded) return res;
                p.Popup(p.playerName + " has the longest road: " + res, "Longest Road");
                playerAtTurn.UpdatePlayerUI();
            }
            else if (res > longestRoadLength && p != playerLongestRoad)
            {
                playerLongestRoad.AddVictoryPoints(-2);
                playerLongestRoad = p;
                playerLongestRoad.AddVictoryPoints(2);
                if (gameEnded) return res;
                p.Popup(p.playerName + " has the longest road: " + results.Max(), "Longest Road");
                playerAtTurn.UpdatePlayerUI();
            }
        }
        playerAtTurn.UpdatePlayerUI();
        return res;
    }
    
    /*-----------------------------------------------------------------------*/
    /*----------------[ Board Setup / Tiles & Intersections ]----------------*/
    /*-----------------------------------------------------------------------*/

    private void AddTile(Tile tile)
    {
        if (!tiles.Contains(tile)) 
            tiles.Add(tile);
    }
    private void RemoveTile(Tile tile)
    {
        tiles.Remove(tile);
    }

    public void GetAllTiles()
    {
        tiles.Clear();

        foreach (Tile tile in transform.Find("Tiles").GetComponentsInChildren<Tile>())
            tiles.Add(tile);
    }

    public Tile getTileByCoordinates(Vector3Int coordinate)
    {
        return tiles.FirstOrDefault(tile => tile.gridCoordinates == coordinate);
    }

    public Tile getNeighbouringTile(Tile t, Vector3Int offset)  // The hex grid system can work a bit unintuitive, this function is to make it easier to get a neighbouring tile
    {   
        // (-1,1) top left
        // (0,1) top right
        // (1,0) right
        // (0,-1) bottom right
        // (-1,-1) bottom left
        // (-1,0) left
        Vector3Int coordinates = t.gridCoordinates;
        if (coordinates.y % 2 == 0 || offset.y == 0)
            coordinates += offset;
        else
            coordinates += offset + new Vector3Int(1, 0, 0);
        return getTileByCoordinates(coordinates);
    }
    
    public void GetAllIntersections()
    {
        intersections.Clear();
        foreach (Intersection intersection in transform.Find("Intersections").GetComponentsInChildren<Intersection>())
            intersections.Add(intersection);
    }

    public void MakeAllIntersections()
    {
        GetAllTiles();

        //first delete all the old intersections
        Transform iParent = transform.Find("Intersections");
        GameObject[] allIntersections = new GameObject[iParent.childCount];
        for (int i = 0; i < allIntersections.Length; i++)
            allIntersections[i] = iParent.GetChild(i).gameObject;
        foreach (GameObject child in allIntersections)
            DestroyImmediate(child);

        int id = 0;
        foreach (Tile tile in tiles)
        {
            Vector3 position = tile.transform.position + new Vector3(-2.5f * Mathf.Sqrt(3), 1, 2.5f);
            // If the tiles to the top left and left are not null make a intersection to the topleft of the tile
            if (getNeighbouringTile(tile, new Vector3Int(-1, 1, 0)) != null &&
                getNeighbouringTile(tile, new Vector3Int(-1, 0, 0)) != null)
            {
                tile.surroundingIntersections[0] = Instantiate(intersectionPrefab, position, Quaternion.identity, iParent).GetComponent<Intersection>();
                tile.surroundingIntersections[0].name = "Intersection " + id++;
                tile.surroundingIntersections[0].surroundingTiles[0] = tile;
            }

            // If the tiles to the top left and top right are not null make a intersection to the topleft of the tile
            if (getNeighbouringTile(tile, new Vector3Int(-1, 1, 0)) != null &&
                getNeighbouringTile(tile, new Vector3Int(0, 1, 0)) != null)
            {
                position = tile.transform.position + new Vector3(0, 1, 5);
                tile.surroundingIntersections[1] = Instantiate(intersectionPrefab, position, Quaternion.identity, iParent).GetComponent<Intersection>();
                tile.surroundingIntersections[1].name = "Intersection " + id++;
                tile.surroundingIntersections[1].surroundingTiles[0] = tile;
            }
            
            
            Tile tileLeft = getNeighbouringTile(tile, new Vector3Int(-1, 0, 0));
            Tile tileTopLeft = getNeighbouringTile(tile, new Vector3Int(-1, 1, 0));
            Tile tileTopRight = getNeighbouringTile(tile, new Vector3Int(0, 1, 0));
            // Make references to the intersections we made above in the surrounding tiles
            if (tileLeft != null)
            {
                tileLeft.surroundingIntersections[2] = tile.surroundingIntersections[0];
                if (tile.surroundingIntersections[0] != null)
                    tile.surroundingIntersections[0].surroundingTiles[1] = tileLeft;
            }
            if (tileTopLeft != null)
            {
                tileTopLeft.surroundingIntersections[4] = tile.surroundingIntersections[0];
                tileTopLeft.surroundingIntersections[5] = tile.surroundingIntersections[1];
                if (tile.surroundingIntersections[0] != null)
                    tile.surroundingIntersections[0].surroundingTiles[2] = tileTopLeft;
                if (tile.surroundingIntersections[1] != null)
                    tile.surroundingIntersections[1].surroundingTiles[1] = tileTopLeft;
            }
            if (tileTopRight != null)
            {
                tileTopRight.surroundingIntersections[3] = tile.surroundingIntersections[1];
                if (tile.surroundingIntersections[1] != null)
                    tile.surroundingIntersections[1].surroundingTiles[2] = tileTopRight;
            }
        }
        
        GetAllIntersections();
        MakeAllStreets();
        MakeAllHarbors();
    }

    public void GetAllStreets()
    {
        streets.Clear();
        foreach (Street s in transform.Find("Streets").GetComponentsInChildren<Street>())
            streets.Add(s);
    }
    
    public void MakeAllStreets()
    {
        //first delete the old streets, if there are any
        Transform sParent = transform.Find("Streets");
        GameObject[] allStreets = new GameObject[sParent.childCount];
        for (int i = 0; i < allStreets.Length; i++)
            allStreets[i] = sParent.GetChild(i).gameObject;
        foreach (GameObject child in allStreets)
            DestroyImmediate(child);

        int id = 0;
        foreach (Tile t in tiles)
        {
            Vector3 pos = t.transform.position;
            // First create street to topleft of tile
            Tile neighbour1 = getNeighbouringTile(t, new Vector3Int(-1, 1, 0));
            if (t.type != TileType.Water 
                || (neighbour1?.type != TileType.Water && neighbour1 != null))
            {
                Vector3 pos1 = pos + new Vector3(-2.1649415f,1,3.75f);
                Quaternion rot1 = Quaternion.Euler(0,60,0);
                Street s = Instantiate(streetPrefab, pos1, rot1, sParent).GetComponent<Street>();
                s.name = "Street " + id++;
                t.surroundingIntersections[0].streets[1] = s;
                t.surroundingIntersections[1].streets[2] = s;
                s.intersections[0] = t.surroundingIntersections[0];
                s.intersections[1] = t.surroundingIntersections[1];
            }
            // Secondly create street to left of tile
            Tile neighbour2 = getNeighbouringTile(t, new Vector3Int(-1, 0, 0));
            if (t.type != TileType.Water 
                || (neighbour2?.type != TileType.Water && neighbour2 != null))
            {
                Vector3 pos2 = pos + new Vector3(-4.329883f, 1, 0);
                Quaternion rot2 = Quaternion.Euler(0,0,0);
                Street s = Instantiate(streetPrefab, pos2, rot2, sParent).GetComponent<Street>();
                s.name = "Street " + id++;
                t.surroundingIntersections[0].streets[2] = s;
                t.surroundingIntersections[3].streets[0] = s;
                s.intersections[1] = t.surroundingIntersections[0];
                s.intersections[0] = t.surroundingIntersections[3];
            }
            // Then create a street bottom left of the tile
            Tile neighbour3 = getNeighbouringTile(t, new Vector3Int(-1, -1, 0));
            if (t.type != TileType.Water 
                || (neighbour3?.type != TileType.Water && neighbour3 != null))
            {
                Vector3 pos3 = pos + new Vector3(-2.1649415f,1,-3.75f);
                Quaternion rot3 = Quaternion.Euler(0,-60,0);
                Street s = Instantiate(streetPrefab, pos3, rot3, sParent).GetComponent<Street>();
                s.name = "Street " + id++;
                t.surroundingIntersections[3].streets[1] = s;
                t.surroundingIntersections[4].streets[0] = s;
                s.intersections[1] = t.surroundingIntersections[3];
                s.intersections[0] = t.surroundingIntersections[4];
            }
        }
        
        GetAllStreets();
    }

    public void RandomizeTileResources()
    {
        GetAllTiles();
        
        List<Tile> resourceTiles = new List<Tile>();
        IDictionary<TileType, int> resAmounts = new Dictionary<TileType, int>();
        resAmounts.Add(TileType.Wood, 0);
        resAmounts.Add(TileType.Stone, 0);
        resAmounts.Add(TileType.Wool, 0);
        resAmounts.Add(TileType.Wheat, 0);
        resAmounts.Add(TileType.Ore, 0);
        foreach (Tile t in tiles.Where(t => t.type != TileType.Water && t.type != TileType.Empty))
        {
            resourceTiles.Add(t);
            resAmounts[t.type]++;
        }

        while (resourceTiles.Count > 0)
        {
            TileType highest = resAmounts.Aggregate((x, y) => x.Value > y.Value ? x : y).Key;  // Gets resource with the largest number of tiles. We do this simply so that we never have to check for zero while emptying the list.
            int index = Random.Range(0, resourceTiles.Count);
            StartCoroutine(resourceTiles[index].SetToType(highest));
            resourceTiles.Remove(resourceTiles[index]);  // This tile no longer needs a resource
            resAmounts[highest]--;
        }
        EditorApplication.QueuePlayerLoopUpdate();  // When just pressing the button in edit mode, the screen will not immediately update; this will force it to.  // Comment this line out when building
    }

    public void AssignTileNumbers(Tile firstTile)  // firstTile should be top left corner tile
    {
        GetAllTiles();
        
        // make all tile numbers empty first
        foreach (Tile t in tiles)
            t.number = 0;
        // queue was used to easily pick the first number in the queue each time
        Queue<int> numbers2 = new Queue<int>(new List<int> { 5, 2, 6, 3, 8, 10, 9, 12, 11, 4, 8, 10, 9, 4, 5, 6, 3, 11 });
        // Order of directions the numbers will be assigned
        Vector3Int[] directions =
        {
            new Vector3Int(-1, -1, 0), new Vector3Int(0, -1, 0), new Vector3Int(1, 0, 0), new Vector3Int(0, 1, 0),
            new Vector3Int(-1, 1, 0), new Vector3Int(-1, 0, 0)
        };
        // List that keeps tracks of which tiles have already had a number assigned
        List<Tile> assignedTiles = new List<Tile>(); 
        Tile currentTile = firstTile;
        // Current direction
        int directionIndex = 0;
        while (numbers2.Count > 0)
        {
            if (!assignedTiles.Contains(currentTile))
            {
                assignedTiles.Add(currentTile);
                if (currentTile.type != TileType.Empty)  // Empty tile should not be assigned a number
                {
                    currentTile.number = numbers2.Dequeue();
                    currentTile.UpdateNumber();
                }
            }

            Tile nexttile = getNeighbouringTile(currentTile, directions[directionIndex]);
            // Check if there is a tile in the current direction otherwise change direction
            if (nexttile?.type != TileType.Water && !assignedTiles.Contains(nexttile)) 
                currentTile = nexttile;
            else
                directionIndex = directionIndex < 5 ? directionIndex + 1 : 0;
        }
    }

    public void MakeAllHarbors()
    {
        GetAllIntersections();
        GetAllStreets();
        
        // First delete all the old harbors
        Transform hParent = transform.Find("Harbors");
        GameObject[] allHarbors = new GameObject[hParent.childCount];
        for (int i = 0; i < allHarbors.Length; i++)
            allHarbors[i] = hParent.GetChild(i).gameObject;
        foreach (GameObject child in allHarbors)
            DestroyImmediate(child);
        foreach (Intersection intersection in intersections)
            intersection.harbor = HarborType.None;
        
        for (int i = 0; i < harborLocations.Count; i++)
        {
            Transform harbor = Instantiate(HarborPrefabs[(int)harborTypes[i] - 1], harborLocations[i].transform.position, Quaternion.identity, hParent).transform;
            harbor.name = harborTypes[i].ToString();
            int armRotation = 0;
            switch (harborConnections[i])
            {
                case 0:
                case 1:
                case 2:
                    armRotation = (2 - harborConnections[i]) * 60;
                    break;
                case 3:
                case 4:
                case 5:
                    armRotation = (6 - harborConnections[i]) * -60;
                    break;
            }
            harbor.Find("Connections Canvas").Rotate(0, 0, armRotation);

            harborLocations[i].surroundingIntersections[harborConnections[i]].harbor = harborTypes[i];
            harborLocations[i].surroundingIntersections[Tile.ClockwiseNextIndex(harborConnections[i])].harbor = harborTypes[i];
        }
    }

    public IEnumerator Rehighlight()
    {
        UnhighlightIntersections();
        UnhighlightStreets();

        yield return new WaitForEndOfFrame();

        switch (phase)
        {
            case Phase.BUILDING:
                HighlightIntersections(playerAtTurn, true, true);
                HighlightStreets(playerAtTurn, true, true);
                break;
            case Phase.INITIALPLACEMENT:
                if (playerAtTurn.initialSettlement)
                    HighlightIntersections(playerAtTurn, false, false);
                if (playerAtTurn.initialRoad)
                    HighlightStreets(playerAtTurn, false, false);
                break;
            case Phase.FREEROADS:
                HighlightStreets(playerAtTurn, false, true);
                break;
        }
    }
    
    private void HighlightIntersections(Player p, bool checkResources, bool checkBuildingsLeft)
    {
        foreach (Intersection intersection in intersections.Where(intersection => intersection.SettlementCanBeBuilt(p, checkResources, checkBuildingsLeft) || intersection.CityCanBeBuilt(p)))
            intersection.particles.Play();
    }
    private void UnhighlightIntersections()
    {
        foreach (Intersection intersection in intersections.Where(intersection => intersection.particles.isPlaying))
            intersection.particles.Stop();
    }
    private void HighlightStreets(Player p, bool checkResources, bool checkRoadsLeft)
    {
        foreach (Street street in streets.Where(street => street.StreetCanBeBuilt(p, checkResources, checkRoadsLeft)))
            street.particles.Play();
    }
    private void UnhighlightStreets()
    {
        foreach (Street street in streets.Where(street => street.particles.isPlaying))
            street.particles.Stop();
    }
    
    /*-----------------------------------------------------------------------*/
    /*-------------------------[ Development Cards ]-------------------------*/
    /*-----------------------------------------------------------------------*/

    public List<DevelopmentCard> devCardStack = new List<DevelopmentCard>();
    public bool devCardPlayed = false;
    public bool knightPlayed = false;

    private void FillDevCardStack()
    {
        // 14 knight cards
        for (int knights = 0; knights < 14; knights++)
        {
            devCardStack.Add(new KnightCard());
        }
        // Each structure card
        devCardStack.Add(new VictoryCard(VictoryCard.Structure.UNIVERSITY));
        devCardStack.Add(new VictoryCard(VictoryCard.Structure.MARKET));
        devCardStack.Add(new VictoryCard(VictoryCard.Structure.GREAT_HALL));
        devCardStack.Add(new VictoryCard(VictoryCard.Structure.CHAPEL));
        devCardStack.Add(new VictoryCard(VictoryCard.Structure.LIBRARY));
        // 2 of each progress card
        devCardStack.Add(new RoadsCard());
        devCardStack.Add(new RoadsCard());
        devCardStack.Add(new PlentyCard());
        devCardStack.Add(new PlentyCard());
        devCardStack.Add(new MonopolyCard());
        devCardStack.Add(new MonopolyCard());
    }

    public void BuyDevCard()
    {
        if (devCardStack.Count == 0)
        {
            playerAtTurn.Popup("There are no development cards left in the bank.", "Development");
            return;
        }
        Player p = playerAtTurn;
        if (p.HasResources(ResourceSet.devcardCost))
        {
            int index = UnityEngine.Random.Range(0, devCardStack.Count);
            DevelopmentCard pulledCard = devCardStack[index];
            p.devCards.Add(pulledCard);
            devCardStack.Remove(pulledCard);
            p.AddResources(ResourceSet.devcardCost.Negative);
            p.UpdateDevCards();
        }
        else
            p.NotEnoughResources();
    }

    public void Plenty(bool discarding, int cardAmount)
    {
        plenty.takeCards = discarding;
        plenty.resourcesLeft = plenty.amountToPick = cardAmount;
        plenty.UpdateTexts();
        if (discarding)
        {         
            discardingPlayer.ShowExpandedResourcesUI(false);
            discardingPlayer.ShowExpandableResourcesUI(true);
            discardingPlayer.ResetOpenIcon();
            discardingPlayer.DevActionPanel(1);
            discardingPlayer.UpdateResourcesUI();
        }
        else playerAtTurn.DevActionPanel(1);
        
    }

    public void PlentyExecute(int wood, int stone, int wool, int wheat, int ore)
    {
        Player p = plenty.takeCards ? discardingPlayer : playerAtTurn;
        p.AddResources(wood, stone, wool, wheat, ore);
        p.DevActionPanel(1);
        playerAtTurn.UpdateResourcesUI();
        if (plenty.takeCards) CheckForDiscard();
        if (PlayPhysical)
            StartCoroutine(Rehighlight());
    }
    public void MonopolyExecute(TileType resource)
    {
        Player receiver = playerAtTurn;
        foreach (Player victim in players.Where(player => player != receiver))
            while (victim.resources[resource] > 0)
            {
                victim.resources[resource]--;
                receiver.resources[resource]++;
            }
        playerAtTurn.DevActionPanel(0);
        playerAtTurn.Popup(receiver.playerName + " has taken all " + resource + " cards from all players!", "Development");
        if (PlayPhysical)
        {
            playerAtTurn.UpdateResourcesUI();
            StartCoroutine(Rehighlight());
        }
    }

    public void FreeRoads()
    {
        phase = Phase.FREEROADS;
        if (PlayPhysical)
            StartCoroutine(Rehighlight());
    }

    /*------------------------------------------------------------------------*/
    /*------------------------------[ Trading ]-------------------------------*/
    /*------------------------------------------------------------------------*/

    public void PlayerTrade(IDictionary<TileType, int> playerGive, IDictionary<TileType, int> playerReceive)  // When playerAtTurn has proposed a player trade
    {
        // From perspective of player that proposed
        foreach (TileType i in Enum.GetValues(typeof(TileType)))
        {
            playerAtTurn.proposedTradeGive[i] = playerGive[i];
            playerAtTurn.proposedTradeReceive[i] = playerReceive[i];
        }

        ProposePlayerTrade();
    }

    public void ProposePlayerTrade()  // Called for every player but playerAtTurn
    {
        if (playerTradingNumber < 3)
            playerTradingNumber++;
        else
        {
            playerTradingNumber = -1;
            playerAtTurn.ProposeFinalTrade();  
            return;
        }
        if (players[playerTradingNumber] == playerAtTurn)
            if (playerTradingNumber < 3)
                playerTradingNumber++;
            else
            {
                playerTradingNumber = -1;
                playerAtTurn.ProposeFinalTrade();
                return;
            }
        players[playerTradingNumber].ProposeTrade();
    }

    public void ProcessPlayerTrade(Player acceptedPlayer)  // Called when playerAtTurn has chosen 1 of the players that confirmed the proposed player trade
    {
        // Wood, Stone, Wool, Wheat, Ore
        playerAtTurn.AddResources(-playerAtTurn.proposedTradeGive[TileType.Wood], -playerAtTurn.proposedTradeGive[TileType.Stone], -playerAtTurn.proposedTradeGive[TileType.Wool], -playerAtTurn.proposedTradeGive[TileType.Wheat], -playerAtTurn.proposedTradeGive[TileType.Ore]);
        playerAtTurn.AddResources(playerAtTurn.proposedTradeReceive[TileType.Wood], playerAtTurn.proposedTradeReceive[TileType.Stone], playerAtTurn.proposedTradeReceive[TileType.Wool], playerAtTurn.proposedTradeReceive[TileType.Wheat], playerAtTurn.proposedTradeReceive[TileType.Ore]);
        acceptedPlayer.AddResources(playerAtTurn.proposedTradeGive[TileType.Wood], playerAtTurn.proposedTradeGive[TileType.Stone], playerAtTurn.proposedTradeGive[TileType.Wool], playerAtTurn.proposedTradeGive[TileType.Wheat], playerAtTurn.proposedTradeGive[TileType.Ore]);
        acceptedPlayer.AddResources(-playerAtTurn.proposedTradeReceive[TileType.Wood], -playerAtTurn.proposedTradeReceive[TileType.Stone], -playerAtTurn.proposedTradeReceive[TileType.Wool], -playerAtTurn.proposedTradeReceive[TileType.Wheat], -playerAtTurn.proposedTradeReceive[TileType.Ore]);
        
        ResetTradeInfo();     
    }

    public void ResetTradeInfo()
    {
        foreach (TileType i in Enum.GetValues(typeof(TileType)))
        {
            playerAtTurn.proposedTradeGive[i] = 0;
            playerAtTurn.proposedTradeReceive[i] = 0;
        }
        ui.pm.GetComponent<ProposalManager>().ResetPanel();
        turnBlock = false;
    }

    public void ProcessBankTrade(IDictionary<TileType, int> bankGive, IDictionary<TileType, int> bankReceive)  // Called when player trades with bank
    {
        // Processing of the bank trade
        playerAtTurn.AddResources(-bankGive[TileType.Wood], -bankGive[TileType.Stone], -bankGive[TileType.Wool], -bankGive[TileType.Wheat], -bankGive[TileType.Ore]);
        playerAtTurn.AddResources(bankReceive[TileType.Wood], bankReceive[TileType.Stone], bankReceive[TileType.Wool], bankReceive[TileType.Wheat], bankReceive[TileType.Ore]);
    }

    public static bool CheckEmpty()
    {
        return Enum.GetValues(typeof(TileType)).Cast<TileType>().Count(i => FindObjectOfType<Board>().playerAtTurn.proposedTradeGive[i] == 0 && FindObjectOfType<Board>().playerAtTurn.proposedTradeReceive[i] == 0) == 7;
    }

    public void DismissPopup()
    {
        playerAtTurn.DismissPopup();
    }
}
