using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using System;
using System.Linq;

public abstract class SelectableObject : Selectable
{
    protected IDictionary<TileType, int> resourceCost = new Dictionary<TileType, int>();
    [SerializeField] private List<TileType> neededResources;

    [HideInInspector] [SerializeReference] public Player player;
    public ParticleSystem particles { get; private set; }

    protected override void Awake()
    {
        board = FindObjectOfType<Board>();
        confirmButton = GameObject.Find("Button - Confirm");
        confirmScript = confirmButton.GetComponent<Confirm>();
        selectable = this;
        
        if (Board.PlayPhysical)
            particles = transform.Find("Selectable Particles").GetComponent<ParticleSystem>();

        foreach (TileType i in Enum.GetValues(typeof(TileType)))
            resourceCost.Add(i, neededResources.Contains(i) ? 1 : 0);
    }

    private void OnMouseDown()  // When clicked on a selectable GameObject
    {
        switch (board.phase)
        {
            // If in building phase
            case Phase.BUILDING when !board.playerAtTurn.ui.confirmButton.activeInHierarchy && !board.inUI && BuildingCanBeBuilt():
                StartCoroutine(confirmScript.ShowConfirmButton());
                confirmScript.CurrentSelectable = selectable;
                StartCoroutine(ShowResourceCosts());
                break;
            // If robber is placed and playerAtTurn has to choose a player to target
            case Phase.CHOOSEVICTIM when player != board.playerAtTurn && player != null && !board.playerAtTurn.ui.confirmButton.activeInHierarchy && !board.playerAtTurn.ui.popupPanel.activeInHierarchy:
                foreach (Intersection i in board.robberTile.surroundingIntersections.Where(i => i == this))
                {
                    StartCoroutine(confirmScript.ShowConfirmButton());
                    confirmScript.CurrentSelectable = selectable;
                }
                break;
            //If in phase where player doens't lose any resources when building things
            case Phase.INITIALPLACEMENT:
            case Phase.FREEROADS:
                if (!board.playerAtTurn.ui.popupPanel.activeInHierarchy)
                    Confirmed();
                break;
        }
    }

    private IEnumerator ShowResourceCosts()
    {
        yield return new WaitForSeconds(0.1f);
        board.playerAtTurn.ShowResourceCosts(resourceCost);
    }

    public abstract bool BuildingCanBeBuilt(); //Checks if Confirm Button needs to appear when clicked on a Selectable GameObject
}
