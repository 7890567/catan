using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;
using TMPro;

public enum TileType { Empty, Water, Wood, Stone, Wool, Wheat, Ore }  // Empty can be used for robber . These are in the same order as HarborType.

[ExecuteInEditMode]
public class Tile : MonoBehaviour
{
    //public static GridLayout Grid;
    
    public TileType type;
    public bool hasRobber;
    [Range(0, 12)] public int number;  // must be 0 if it's Empty or Water
    //The following indexes represent the following intersections in this array
    // 0: top left
    // 1: top middle
    // 2: top right
    // 3: bottom left
    // 4: bottom middle
    // 5: bottom right
    public Intersection[] surroundingIntersections = new Intersection[6];

    public Vector3Int gridCoordinates;// => Grid.WorldToCell(transform.position);  // Please don't change this value in the editor, we can't make it read-only and we do want to view it :)

    public int IntersectionCount => surroundingIntersections.Count(intersection => intersection != null);

    private Board board;
    private TextMeshProUGUI numberText;

    public static event Action<Tile> TileCreated;
    private void Awake()
    {
        //Grid ??= FindObjectOfType<GridLayout>();//GameObject.Find("Tiles").GetComponent<GridLayout>();  // Unity does not allow this to be initialized at the variable declaration or in a static constructor
        
        if (PrefabUtility.IsOutermostPrefabInstanceRoot(gameObject)) PrefabUtility.UnpackPrefabInstance(gameObject, PrefabUnpackMode.OutermostRoot, InteractionMode.AutomatedAction);  // Comment this line out when building
        TileCreated?.Invoke(this);

        board = FindObjectOfType<Board>();//FindObjectOfType<Board>();
        numberText = GetComponentInChildren<TextMeshProUGUI>();
        UpdateNumber();

        IEnumerator Coords()
        {
            yield return null;//new WaitForEndOfFrame();
            GridLayout gridLayout = transform.GetComponentInParent<GridLayout>();
            gridCoordinates = gridLayout.WorldToCell(transform.position);  // This was the old way of doing it, but I discovered it didn't work in edit mode so I made it a property. Then I discovered yield return null works to wait for end of frame in edit mode so I changed it back to using this.
        }
        StartCoroutine(Coords());
    }

    private void OnValidate()
    {
        if (name != type.ToString()) StartCoroutine(SetToType(type));
        UpdateNumber();
    }

    public void OnMouseDown()
    {
        if (board.inUI) return;
        if (board.phase == Phase.ROBBER && type != TileType.Water)
        {
            board.playerAtTurn.HidePopupInstructions();
            board.MoveRobber(this);
        }         
    }

    public IEnumerator SetToType(TileType newType)
    {
        yield return null;//new WaitForEndOfFrame();
        
        type = newType;
        DestroyImmediate(transform.Find("Hexagon").gameObject);
        Transform newHexagon = Instantiate(board.TilePrefabs[(int)newType].transform.Find("Hexagon"), transform);
        /* = PrefabUtility.LoadPrefabContents(AssetDatabase.GetAssetPath(Board.TilePrefabs[newType].transform.Find("Hexagon")));
        newHexagon.transform.parent = transform;
        newHexagon.transform.localPosition = Vector3.zero;*/
        newHexagon.name = "Hexagon";
        name = type.ToString();
    }

    public static event Action<Tile> TileDestroyed;
    private void OnDestroy()
    {
        TileDestroyed?.Invoke(this);
    }

    public void NumberRolled()
    {
        foreach (Intersection i in surroundingIntersections)
            i.GiveResources(type);
    }

    public void UpdateNumber()
    {
        numberText?.SetText(number.ToString());
    }

    public static int ClockwiseNextIndex(int i)  // Returns -1 if the given index was invalid
    {
        switch (i)
        {
            case 0:
            case 1:
                return i + 1;
            case 2:
                return 5;
            case 3:
                return 0;
            case 4:
            case 5:
                return i - 1;
            default:
                return -1;
        }
    }
}
