using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;
using Object = UnityEngine.Object;

[Serializable]
public class Player
{
    public string playerName;
    public Color color;

    //Resource list of Player
    public IDictionary<TileType, int> resources = new Dictionary<TileType, int>
    {
        { TileType.Wood, 0 },
        { TileType.Stone, 0 },
        { TileType.Wool, 0 },
        { TileType.Wheat, 0 },
        { TileType.Ore, 0 }
    };
    public List<HarborType> harbors = new List<HarborType>();
    public List<DevelopmentCard> devCards = new List<DevelopmentCard>();
    
    public bool AtTurn => board.playerAtTurn == this;

    // Declaration of Inventory
    protected int maxRoads = 15, maxSettlements = 5, maxCities = 4;
    public int roadsLeft, settlementsLeft, citiesLeft, freeRoads, devCardsPlayed;  // These are assigned in the constuctor

    public int victoryPoints, knightsUsed, roadLength;  // These default to 0

    public bool initialSettlement, initialRoad;
    public bool preDicePopup, preTradePopup, popupUI;

    public Board board;
    public UIHandler ui;

    public MaterialPropertyBlock mpb;

    public IDictionary<TileType, int> proposedTradeGive = new Dictionary<TileType, int>();
    public IDictionary<TileType, int> proposedTradeReceive = new Dictionary<TileType, int>();

    public Player(Board b, UIHandler h, Color c, string s)
    {
        board = b;
        ui = h;
        color = c;
        playerName = s;

        mpb = new MaterialPropertyBlock();
        mpb.SetColor("_Color", c);

        roadsLeft = maxRoads;
        settlementsLeft = maxSettlements;
        citiesLeft = maxCities;
        devCardsPlayed = 0;  // This variable is used for the AI choice balancing
        initialSettlement = false;
        initialRoad = false;

        foreach (TileType i in Enum.GetValues(typeof(TileType)))
        {
            proposedTradeGive.Add(i, 0);
            proposedTradeReceive.Add(i, 0);
        }
    }
    
    public void OnValidate()
    {
        if (mpb?.GetColor("_Color") == color) return;
        mpb = new MaterialPropertyBlock();
        mpb.SetColor("_Color", color);
    }

    public virtual void StartTurn()
    {
        Popup(playerName + " is at turn now.", "Turn has Changed");
        NextPlayerUI();
        preDicePopup = true;
        board.diceThrown = false;
    }

    public virtual void Robber()
    {
        // Empty on purpose: for now, only the AI needs it, but it is called for every player, so it must exist here
    }

    public virtual void Trading()
    {
        // Empty on purpose: for now, only the AI needs it, but it is called for every player, so it must exist here
    }
    public virtual void Development()
    {
        // Empty on purpose: for now, only the AI needs it, but it is called for every player, so it must exist here
    }
    public virtual void EndTurn()
    {
        // Empty on purpose: for now, only the AI needs it, but it is called for every player, so it must exist here
    }
    public void AddVictoryPoints(int amount)
    {
        victoryPoints += amount;
        UpdatePlayerUI();
        if (victoryPoints >= 10) WinGame(false);
        if (victoryPoints + CountDevClass(DevelopmentCard.CardType.VICTORY) >= 10) WinGame(true);
    }

    // When the Player has won the game
    protected virtual void WinGame(bool victoryCardsUsed)
    {
        board.gameEnded = true;
        if (victoryCardsUsed) Popup(playerName + " has got enough victory points!\n" + playerName + " wins the game!", "Victory");
        else Popup(playerName + " has got enough victory cards to get 10 points!\n" + playerName + " wins the game!", "Victory");
        for (int i = 0; i < 100; i++) ThrowDice();
    }

    public void AddResources(int wood, int stone, int wool, int wheat, int ore)
    {
        resources[TileType.Wood] += wood;
        resources[TileType.Stone] += stone;
        resources[TileType.Wool] += wool;
        resources[TileType.Wheat] += wheat;
        resources[TileType.Ore] += ore;
        if (AtTurn)
            UpdateResourcesUI();
    }


    public void AddResources(ResourceSet resourceSet)
    {
        resources[TileType.Wood] += resourceSet.wood;
        resources[TileType.Stone] += resourceSet.stone;
        resources[TileType.Wool] += resourceSet.wool;
        resources[TileType.Wheat] += resourceSet.wheat;
        resources[TileType.Ore] += resourceSet.ore;
        if (AtTurn)
            UpdateResourcesUI();
    }

    public bool HasResources(int wood, int stone, int wool, int wheat, int ore)
    {
        return resources[TileType.Wood] >= wood && resources[TileType.Stone] >= stone && resources[TileType.Wool] >= wool && resources[TileType.Wheat] >= wheat && resources[TileType.Ore] >= ore;
    }

    public bool HasResources(ResourceSet resourceSet)
    {
        return resources[TileType.Wood] >= resourceSet.wood && resources[TileType.Stone] >= resourceSet.stone && resources[TileType.Wool] >= resourceSet.wool && resources[TileType.Wheat] >= resourceSet.wheat && resources[TileType.Ore] >= resourceSet.ore;
    }

    public int ResourceAmount => resources[TileType.Wood] + resources[TileType.Stone] + resources[TileType.Wool] + resources[TileType.Wheat] + resources[TileType.Ore];
    public ResourceSet ResourceAmounts => new ResourceSet(resources[TileType.Wood], resources[TileType.Stone], resources[TileType.Wool], resources[TileType.Wheat], resources[TileType.Ore]);

    public virtual void ProposeTrade()  // Does all the preparing work for displaying the right UI
    {
        UpdateResourcesUI();
        ShowExpandedResourcesUI(false);
        ShowExpandableResourcesUI(true);
        ResetOpenIcon();
        board.turnBlock = true;

        //iField0 = Wheat
        //iField1 = Wood
        //iField2 = Stone
        //iField3 = Wool
        //iField4 = Ore

        foreach (GameObject TMP_if in ui.iFieldsGive)
            TMP_if.GetComponent<TextMeshProUGUI>().text = "";
        foreach (GameObject TMP_if in ui.iFieldsReceive)
            TMP_if.GetComponent<TextMeshProUGUI>().text = "";

        foreach (TileType i in Enum.GetValues(typeof(TileType)))
        {
            foreach (GameObject TMP_if in ui.iFieldsGive)
                if (TMP_if.CompareTag(i.ToString()) && board.playerAtTurn.proposedTradeReceive[i] > 0)
                    TMP_if.GetComponent<TextMeshProUGUI>().text = board.playerAtTurn.proposedTradeReceive[i].ToString();
                
            foreach (GameObject TMP_if in ui.iFieldsReceive)
                if (TMP_if.CompareTag(i.ToString()) && board.playerAtTurn.proposedTradeGive[i] > 0)
                       TMP_if.GetComponent<TextMeshProUGUI>().text = board.playerAtTurn.proposedTradeGive[i].ToString();              
        }

        ui.tradeProposalHeader.GetComponent<Image>().color = color;
        ui.proposalConfirmButton.player = this;
        ui.proposalDeclineButton.player = this;

        Popup(playerName + " is now reacting to the trade proposal.", "Trading");
    }

    public virtual void ProposeFinalTrade()  // Does all the preparing work for displaying the right UI
    {
        UpdateResourcesUI();
        ShowExpandedResourcesUI(false);
        ShowExpandableResourcesUI(true);
        ResetOpenIcon();
        board.turnBlock = true;

        //iField0 = Wheat
        //iField1 = Wood
        //iField2 = Stone
        //iField3 = Wool
        //iField4 = Ore
        foreach (TileType i in Enum.GetValues(typeof(TileType)))
        {
            foreach (GameObject TMP_text in ui.resourcesGive)
                if (TMP_text.CompareTag(i.ToString()))
                    TMP_text.GetComponent<TextMeshProUGUI>().text = "- " + board.playerAtTurn.proposedTradeGive[i].ToString();

            foreach (GameObject TMP_text in ui.resourcesReceive)
                if (TMP_text.CompareTag(i.ToString()))
                    TMP_text.GetComponent<TextMeshProUGUI>().text = board.playerAtTurn.proposedTradeReceive[i].ToString() + " -";
        }       
       
        int playerSkip = 0;
        for (int i = 0; i < 3; i++)
        {
            if (board.players[i] == board.playerAtTurn)
                playerSkip++;

            if (ui.pm.GetComponent<ProposalManager>().playerList[board.players[i + playerSkip]])
                ui.playerButtons[i].GetComponent<Image>().color = board.players[i + playerSkip].color;
            else
            {
                Color tempColor = board.players[i + playerSkip].color;
                tempColor.a = 0.2f;
                ui.playerButtons[i].GetComponent<Image>().color = tempColor;
                tempColor = new Color(0, 0, 0);
                tempColor.a = 0.2f;
                ui.playerButtons[i].transform.Find("Text (TMP)").GetComponent<TextMeshProUGUI>().color = tempColor;
                ui.playerButtons[i].transform.Find("Image - Player Icon").GetComponent<Image>().color = tempColor;
            }

            ui.playerButtonScripts[i].player = board.players[i + playerSkip];
            ui.playerButtonNames[i].text = board.players[i + playerSkip].playerName.ToString();           
        }
        ui.tradeProposalHeader.GetComponent<Image>().color = color;

        Popup(playerName + " can now choose a player to make the trading deal with.", "Trading");
    }

    /*-----------------------------------------------------------------------*/
    /*-------------------------[ Development Cards ]-------------------------*/
    /*-----------------------------------------------------------------------*/

    public void ChosenCard(DevelopmentCard.CardType cardType)
    {
        if (board.devCardPlayed)
        {
            board.playerAtTurn.Popup("You've already played a development card this turn.", "Development");
            return;
        }
        foreach (DevelopmentCard dc in devCards)
            if (cardType == dc.cardtype && cardType == DevelopmentCard.CardType.KNIGHT)
            {
                ShowPopupInstructions("Choose a Tile to target");
                dc.PlayCard();
                return;
            }
        // If the foreach hasn't returned, it means you don't have the card. So:
        Popup("You don't have that card!", "Development Card");
    }

    public void ChosenCard(ProgressCard.Progress cardType)
    {
        if (board.devCardPlayed)
        {
            board.playerAtTurn.Popup("You've already played a development card this turn.", "Development");
            return;
        }
        foreach (DevelopmentCard dc in devCards)
            if (dc.cardtype == DevelopmentCard.CardType.PROGRESS && cardType == ((ProgressCard)dc).progress)
            {
                dc.PlayCard();
                return;
            }
        Popup("You don't have that card!", "Development Card");
    }

    public void AddFreeRoads(int added)
    {
        // First we should check if the player even has 2 roads left to build 
        if (roadsLeft >= freeRoads + added)
            freeRoads += added;
        // If not, then perhaps we could still build 1 road instead of 2
        else
        {
            added--;
            // If there are still free roads left to build, we will give the player their free roads, by retrying this same method with the reduced amount of added roads
            if (added > 0)
                AddFreeRoads(added);
            else
                Popup("You can't build any more streets.", "Development");
        }
    }

    public int CountDevClass(DevelopmentCard.CardType cardtype)
    {
        return devCards.Count(dc => dc.cardtype == cardtype);
    }

    public int CountDevClass(VictoryCard.Structure cardtype)
    {
        return devCards.Count(dc => dc.cardtype == DevelopmentCard.CardType.VICTORY && ((VictoryCard)dc).structure == cardtype);
    }

    public int CountDevClass(ProgressCard.Progress cardtype)
    {
        return devCards.Count(dc => dc.cardtype == DevelopmentCard.CardType.PROGRESS && ((ProgressCard)dc).progress == cardtype);
    }

    /*----------------------------------------------------------------------*/
    /*----------------------------[ UI Display ]----------------------------*/
    /*----------------------------------------------------------------------*/

    public virtual void NotEnoughResources()
    {
        Popup("You don't have the resources to perform this action!", "Development");
    }

    public virtual void Panel(int panelNumber)  // Showing main Panels, 0: Dice, 1: Trading, 2: Devcards
    {
        GameObject panel = ui.panels[panelNumber];
        ui.panelHeaders[panelNumber].GetComponent<Image>().color = color;
        panel.SetActive(!panel.activeSelf);
        board.inUI = !board.inUI;
    }

    public virtual void DevActionPanel(int panelNumber)  // Showing Panels for when a Dev Card is played
    {
        GameObject panel = ui.devActionPanels[panelNumber];
        ui.devActionHeaders[panelNumber].GetComponent<Image>().color = color;
        panel.SetActive(!panel.activeSelf);
        board.turnBlock = !board.turnBlock;
        board.inUI = !board.inUI;
    }

    public virtual void ChooseRobberVictim(Tile tile)
    {
        bool targetPresent = false;
        bool targetResources = false;
        foreach (Intersection i in tile.surroundingIntersections.Where(i => i.player != null))
        {
            if (i.player != board.playerAtTurn) targetPresent = true;
            if (i.player.ResourceAmount > 0) targetResources = true;  // Double if required, 'cause second can't be checked if player is null
        }
        if (!targetPresent || !targetResources)
            board.ReturnFromRobber();
        else
        {
            board.phase = Phase.CHOOSEVICTIM;
            ShowPopupInstructions("Choose a Player to target");
        }
    }

    public virtual void ShowStolenCard(TileType resource)
    {
        Popup("You have stolen the following resource: " + resource, "Robber");
        preTradePopup = true;
    }

    public virtual void ShowExpandedResourcesUI(bool show)
    {
        ui.expandedResourcePanel.SetActive(show);
    }

    public virtual void ShowExpandableResourcesUI(bool show)
    {
        ui.expandableResourcePanel.SetActive(show);
    }

    public virtual void ResetOpenIcon()
    {
        ui.openCardWindowButton.IsOpened = true;
        ui.openCardWindowButton.UpdateIcon();
    }

    public virtual void Popup(string message, string header)  // Popup: multi-purpose
    {
        ui.popupText.text = message;
        // If (board.phase != Phase.INITIALPLACEMENT)
        ui.popupHeader.GetComponent<Image>().color = color;
        ui.popupHeaderText.text = header;
        // ui.popupPanel.SetActive(true);
        if (!board.inUI) popupUI = true;
        board.inUI = true;
        ui.StartCoroutine(ui.ShowPopupPanel());
        if (board.plenty.takeCards)
            try  // Lazy, I know :D
            {
                ui.popupHeader.GetComponent<Image>().color = board.players[board.playerDiscardCounter].color;
            }
            catch
            {
                // Ignored
            }
    }

    public virtual void DismissPopup()
    {
        if (board.gameEnded)
        {
            Scene scene = SceneManager.GetActiveScene(); SceneManager.LoadScene(scene.name);
        }
        ui.popupPanel.SetActive(false);
        if (popupUI) board.inUI = false;
        popupUI = false;
        if (preDicePopup) Panel(0);
        preDicePopup = false;

        if (board.phase == Phase.INITIALPLACEMENT) ShowPopupInstructions("Build a Settlement");
        if (board.phase == Phase.ROBBER) ShowPopupInstructions("Choose a Tile to target");
        if (board.phase == Phase.TRADING && !Board.CheckEmpty() && !ui.tradeProposal.activeInHierarchy && board.playerTradingNumber > -1) ShowProposeTrade();
        if (board.phase == Phase.TRADING && !Board.CheckEmpty() && !ui.tradeProposal.activeInHierarchy && board.playerTradingNumber == -1) ShowConfirmationTrade();
    }

    public virtual void ThrowDice()
    {
        Object.Instantiate(ui.throwableDice[0]);
        Object.Instantiate(ui.throwableDice[1]);
        board.diceThrown = true;
    }

    public virtual void UpdateDiceUI(int die1, int die2)  // Updates all Die images
    {
        ui.dice[0].ShowEyes(die1 - 1);
        ui.dice[1].ShowEyes(die2 - 1);
        ui.StartCoroutine(ui.GiveDiceSides(die1, die2));
    }

    public virtual void ResetView()
    {
        ui.viewResetButton.ResetView();
    }

    public virtual void ResetSmallDice()  // Die images at bottom left corner
    {
        Color changedColor = ui.diceSmall[0].GetComponent<Image>().color;
        changedColor.a = 0f;
        ui.diceSmall[0].GetComponent<Image>().color = changedColor;
        ui.diceSmall[1].GetComponent<Image>().color = changedColor;
    }

    public virtual void ShowResourceCosts(IDictionary<TileType, int> resourceCosts)  // As Popup instruction
    {
        string result = "";
        foreach (TileType tileType in Enum.GetValues(typeof(TileType)))
        {
            if (result == "" && tileType != TileType.Empty && tileType != TileType.Water && resourceCosts[tileType] != 0)
                result += resourceCosts[tileType].ToString() + " " + tileType.ToString();
            else
                if (resourceCosts[tileType] != 0)
                result += ", " + resourceCosts[tileType].ToString() + " " + tileType.ToString();
        }
        ShowPopupInstructions(result);
    }

    public void ShowPopupInstructions(string instruction)  // Popup instruction: multi-purpose
    {
        ui.popupInstructions.GetComponent<TextMeshProUGUI>().text = instruction;
        ui.popupInstructions.SetActive(true);
    }

    public virtual void HidePopupInstructions()
    {
        ui.popupInstructions.GetComponent<TextMeshProUGUI>().text = "";
        ui.popupInstructions.SetActive(false);
    }

    public void ShowProposeTrade()  // Proposal Window + Window where Players can decline & accept trades
    {
        ui.StartCoroutine(ui.ShowTradeProposal());
    }

    private void ShowConfirmationTrade()  // Proposal Window + Window where playerAtTrun can react to other Players willingness to trade
    {
        ui.StartCoroutine(ui.ShowTradeConfirmation());
    }

    /*-----------------------------------------------------------------------*/
    /*----------------------------[ UI Updating ]----------------------------*/
    /*-----------------------------------------------------------------------*/

    public virtual void NextPlayerUI()  // Updates all UI that displays information about the state of the game
    {
        UpdatePhaseUI();
        UpdateResourcesUI();
        UpdateHarborsUI();
        UpdatePlayerUI();
        UpdateStructuresUI();

        ShowExpandedResourcesUI(false);
        ShowExpandableResourcesUI(true);
        ResetOpenIcon();
    }
    
    public virtual void UpdateResourcesUI()
    {
        for (int i = 0; i < 5; i++)
            ui.resourceAmounts[i].text = resources[(TileType)(i + 2)].ToString();
        for (int i = 0; i < 4; i++)
            ui.playerResourceCards[i].text = board.players[i].ResourceAmount.ToString();
        UpdatePlayerUI(); 
    }

    public virtual void UpdateHarborsUI()
    {
        for (int i = 0; i < 6; i++)
        {
            ui.portChecks[i].SetActive(harbors.Contains((HarborType)(i + 1)));
        }
    }
    public virtual void UpdateStructuresUI()
    {
        ui.structureAmounts[0].text = (maxRoads - roadsLeft).ToString() + "/" + maxRoads;
        ui.structureAmounts[1].text = (maxSettlements - settlementsLeft).ToString() + "/" + maxSettlements;
        ui.structureAmounts[2].text = (maxCities - citiesLeft).ToString() + "/" + maxCities;
    }

    public virtual void UpdatePlayerUI()
    {
        for (int i = 0; i < 4; i++)
        {
            ui.playerPoints[i].text = board.players[i].victoryPoints.ToString();
            ui.playerKnightsUsed[i].text = board.players[i].knightsUsed.ToString();
            ui.playerRoadlength[i].text = board.players[i].roadLength.ToString();
            ui.playerVictoryIcons[i].color = Color.black;
            ui.playerResourcesIcons[i].color = Color.black;
            ui.playerKnightsIcons[i].color = Color.black;
            ui.playerRoadIcons[i].color = Color.black;
        }
        try
        {
            ui.playerVictoryIcons[board.playerTurnNumber].color = Color.white;
            ui.playerResourcesIcons[board.playerTurnNumber].color = Color.white;
            ui.playerKnightsIcons[board.playerTurnNumber].color = Color.white;
            ui.playerRoadIcons[board.playerTurnNumber].color = Color.white;
        }
        catch
        {
            ui.playerVictoryIcons[board.actualTurn].color = Color.white;
            ui.playerResourcesIcons[board.actualTurn].color = Color.white;
            ui.playerKnightsIcons[board.actualTurn].color = Color.white;
            ui.playerRoadIcons[board.actualTurn].color = Color.white;
        }
    }

    public virtual void UpdatePhaseUI()
    {
        ui.phases[0].SetActive(false);
        ui.phases[1].SetActive(false);
        ui.phases[2].SetActive(false);
        ui.confirmButton.SetActive(false);
        ui.confirmButton.GetComponent<Confirm>().CurrentSelectable = null;

        if (board.phase == Phase.BUILDING) ui.tm.ResetPanel();

        if (board.phase == Phase.DICE) ui.panels[2].SetActive(false);

        board.inUI = false;
        if (board.phase != Phase.INITIALPLACEMENT) ui.phases[(int)board.phase].SetActive(true);
    }
    public virtual void UpdateDevCards()
    {
        ui.devCardAmounts[0].text = CountDevClass(VictoryCard.Structure.CHAPEL).ToString();
        ui.devCardAmounts[1].text = CountDevClass(VictoryCard.Structure.GREAT_HALL).ToString();
        ui.devCardAmounts[2].text = CountDevClass(VictoryCard.Structure.LIBRARY).ToString();
        ui.devCardAmounts[3].text = CountDevClass(VictoryCard.Structure.MARKET).ToString();
        ui.devCardAmounts[4].text = CountDevClass(VictoryCard.Structure.UNIVERSITY).ToString();
        ui.devCardAmounts[5].text = CountDevClass(DevelopmentCard.CardType.KNIGHT).ToString();
        ui.devCardAmounts[6].text = CountDevClass(ProgressCard.Progress.MONOPOLY).ToString();
        ui.devCardAmounts[7].text = CountDevClass(ProgressCard.Progress.PLENTY).ToString();
        ui.devCardAmounts[8].text = CountDevClass(ProgressCard.Progress.ROADS).ToString();
    }
}

public struct ResourceSet
{
    public int stone;
    public int wood;
    public int wool;
    public int wheat;
    public int ore;
    
    public ResourceSet(int stone, int wood, int wool, int wheat, int ore)
    {
        this.stone = stone;
        this.wood = wood;
        this.wool = wool;
        this.wheat = wheat;
        this.ore = ore;
    }
    
    public static ResourceSet streetCost = new ResourceSet(1, 1, 0, 0, 0);
    public static ResourceSet settlementCost = new ResourceSet(1, 1, 1, 1, 0);
    public static ResourceSet cityCost = new ResourceSet(0, 0, 0, 2, 3);
    public static ResourceSet devcardCost = new ResourceSet(0, 0, 1, 1, 1);
    
    public ResourceSet Negative => new ResourceSet(-stone, -wood, -wool, -wheat, -ore);

    public ResourceSet Remove(ResourceSet res)
    {
        return new ResourceSet(this.stone - res.stone, this.wood - res.wood, this.wool - res.wool, this.wheat - res.wheat, this.ore - res.ore);
    }
}
