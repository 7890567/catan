using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Random = UnityEngine.Random;

public class RuleBasedAi : AI
{
    private List<Intersection> ownedIntersections = new List<Intersection>();
    private List<Street> ownedStreets = new List<Street>();

    public RuleBasedAi(Board b, UIHandler h, Color c, string s) : base(b, h, c, s)
    { }

    private static readonly Dictionary<int, int> probabilities = new Dictionary<int, int>()
    {
        {0,0},
        {2,3}, {12,3},
        {3,6}, {11,6},
        {4,8}, {10,8},
        {5,11}, {9,11},
        {6,14}, {8,14},
        {7,17},
    };

    private Dictionary<TileType, int> incomeRate = new Dictionary<TileType, int>() {{TileType.Wood, 0}, {TileType.Stone, 0}, {TileType.Wool, 0}, {TileType.Wheat, 0}, {TileType.Ore, 0}};

    private TileType lowestResource
    {
        get
        {
            int lowest = 0;
            TileType res = TileType.Wood;  // Assigned this at first so that there is still a return value if al the ores are the same
            for (TileType t = TileType.Wood; t <= TileType.Ore; t++)
                if(incomeRate[t]<lowest)
                {
                    res = t;
                    lowest = incomeRate[t];
                }

            return res;
        }
    }
    
    private enum TargetType { settlement, city, devcard, none }
    private Target target;
    private bool targetChosen;

    private bool disableStreets;
    private bool disableSettlements;
    private bool disableCities;

    // For FindTarget() and CheckStreets() :
    private int distance;
    public int distanceCountdown;
    private IDictionary<Street, int> streetDistances = new Dictionary<Street, int>();
    private List<Street> routeStreets = new List<Street>();

    private IDictionary<TargetType, ResourceSet> costs = new Dictionary<TargetType, ResourceSet>
    {
        { TargetType.settlement, ResourceSet.settlementCost },
        { TargetType.city, ResourceSet.cityCost },
        { TargetType.devcard, ResourceSet.devcardCost },
        { TargetType.none, new ResourceSet(0, 0, 0, 0, 0) }
    };

    private bool monopolyEnabled;
    private bool plentyEnabled;
    private bool discardEnabled;

    private struct Target
    {
        public TargetType targetType;
        public TileType resource;
        public Intersection location;

        public Target(TargetType tt, TileType tr)
        {
            targetType = tt;
            resource = tr;
            location = null;
        }
    }

    // These are required for the AI to determine if it should no longer choose these individual targets
    IDictionary<Target, bool> targetOptions = new Dictionary<Target, bool>
    {
        { new Target(TargetType.settlement, TileType.Stone), false },
        { new Target(TargetType.settlement, TileType.Wood), false },
        { new Target(TargetType.settlement, TileType.Wool), false },
        { new Target(TargetType.settlement, TileType.Wheat), false },
        { new Target(TargetType.settlement, TileType.Ore), false },
        { new Target(TargetType.city, TileType.Stone), false },
        { new Target(TargetType.city, TileType.Wood), false },
        { new Target(TargetType.city, TileType.Wool), false },
        { new Target(TargetType.city, TileType.Wheat), false },
        { new Target(TargetType.city, TileType.Ore), false }
    };

    private TileType ResourceNeeded()
    {
        ResourceSet r = costs[target.targetType].Remove(ResourceAmounts);  // The result is a set of the still required resources
        IDictionary<TileType, int> d = new Dictionary<TileType, int>();
        d.Add(TileType.Stone, r.stone);
        d.Add(TileType.Wood, r.wood);
        d.Add(TileType.Wool, r.wool);
        d.Add(TileType.Wheat, r.wheat);
        d.Add(TileType.Ore, r.ore);

        TileType highest;
        return highest = d.Aggregate((x, y) => x.Value > y.Value ? x : y).Key;
    }

    private Player targetPlayer;  // Used for robber and choosvictim
    
    //used for trading
    private IDictionary<TileType, int> resourcesDict;
    private TileType resourceNeeded;
    private List<TileType> resourcesExcess;
    private double ratio;
    private int resourcesAway;

    public override void InitialPlacement()
    {
        if (board.playerTurnNumber < 4)  // Check if it is the first or the second building being placed
        {
            // First we add all intersections that have 3 different surrounding resources to a single list
            List<Intersection> Intersections3resources = new List<Intersection>();
            foreach (Intersection i in board.intersections)
            {
                if (i.building != BuildingType.Empty || !i.SettlementCanBeBuilt(this, false, false))
                    continue;
                List<TileType> surroundingResources = new List<TileType>();
                for (int j = 0; j < 3; j++)
                    surroundingResources.Add(i.surroundingTiles[j].type);
                int amountOfResources = 0;
                for (TileType t = TileType.Wood; t <= TileType.Ore; t++)
                    if (surroundingResources.Contains(t))
                        amountOfResources++;
                if(amountOfResources > 2)
                    Intersections3resources.Add(i);
            }
            // In case there are no intersections with 3 different resources, just make the list of all available intersections
            if (Intersections3resources.Count == 0)
                Intersections3resources.AddRange(board.intersections.Where(i => i.SettlementCanBeBuilt(this, false, false)));

            Intersection bestIntersection = HighestProbIntersection(Intersections3resources);
            
            bestIntersection.Confirmed();
            ownedIntersections.Add(bestIntersection);
            InitialRoad();
        }
        else
        {
            // First we make a list of all intersections that combined with the previous chosen intersection contain all resources
            List<Intersection> intersectionsAllResources = new List<Intersection>();
            foreach (Intersection i in board.intersections)
            {
                if (i.building != BuildingType.Empty || !i.SettlementCanBeBuilt(this, false, false))
                    continue;
                List<TileType> Resources = new List<TileType>();
                for (int j = 0; j < 3; j++)
                {
                    Resources.Add(i.surroundingTiles[j].type);
                    Resources.Add(ownedIntersections[0].surroundingTiles[j].type);
                }
                bool allResources = true;
                for (TileType t = TileType.Wood; t <= TileType.Ore; t++)
                    if (!Resources.Contains(t))
                        allResources = false;
                if(allResources)
                    intersectionsAllResources.Add(i);
            }
            
            // In case there are no intersections so that all resources are satisfied, just make a list of all possible intersections 
            if (intersectionsAllResources.Count == 0)
                intersectionsAllResources.AddRange(board.intersections.Where(i => i.SettlementCanBeBuilt(this, false, false)));

            Intersection bestIntersection = HighestProbIntersection(intersectionsAllResources);
            bestIntersection.Confirmed();
            ownedIntersections.Add(bestIntersection);
            InitialRoad();
        }
    }

    //calculates the income of every resource
    private void UpdateIncomeRate()
    {
        for (TileType t = TileType.Wood; t <= TileType.Ore; t++)
        {
            incomeRate[t] = 0;
        }
        
        foreach (Tile t in board.tiles)
        {
            if (t.type == TileType.Water)
                continue;

            int tileScore = 0;
            for (int i = 0; i < 6; i++)
            {
                if (t.surroundingIntersections[i].player == this &&
                    t.surroundingIntersections[i].building == BuildingType.Settlement)
                    tileScore ++;
                if (t.surroundingIntersections[i].player == this &&
                    t.surroundingIntersections[i].building == BuildingType.City)
                    tileScore += 2;
            }

            tileScore *= probabilities[t.number];

            if (t.type != TileType.Empty && t.type != TileType.Water)
                incomeRate[t.type] += tileScore;
        }
    }

    private static Intersection HighestProbIntersection(List<Intersection> intersections)
    {
        // Function picks the intersection with the highest average probability to be rolled
        Intersection bestIntersection = null;
        int highestProb = 0;

        foreach (Intersection i in intersections)
        {
            int prob = 0;
            for (int j = 0; j < 3; j++)
                prob += probabilities[i.surroundingTiles[j].number];
            if (prob > highestProb)
            {
                bestIntersection = i;
                highestProb = prob;
            }
        }

        return bestIntersection;
    }

    private void InitialRoad()
    {
        int highestProb = 0;
        Street bestStreet = null;
        // The two initials roads will be placed towards the intersection with the highest average probability
        foreach (Street s in board.streets.Where(s => s.StreetCanBeBuilt(this, false, true)))
        {
            for(int i = 0; i<2; i++)
            for (int j = 0; j < 3; j++)
            {
                int prob = 0;
                for (int k = 0; k < 3; k++)
                    prob += probabilities[s.intersections[i].surroundingTiles[j].number];
                if (prob > highestProb)
                    bestStreet = s;
            }
        }
        bestStreet.Confirmed();
    }

    //called whenever the trading phase begins, ai decides wat resources it wants and what it wants to give away
    public override void Trading()
    {
        UpdateIncomeRate();
        
        if (RouteChanged() || !targetChosen)
            DetermineTarget();
        
        Debug.Log(target.targetType);
        ResourceSet r = costs[target.targetType].Remove(ResourceAmounts); 
        // The result is dictionary where a negative value means there are more resources than required and a positive one means more resources then required
        resourcesDict = new Dictionary<TileType, int>();
        resourcesDict.Add(TileType.Stone, r.stone);
        resourcesDict.Add(TileType.Wood, r.wood);
        resourcesDict.Add(TileType.Wool, r.wool);
        resourcesDict.Add(TileType.Wheat, r.wheat);
        resourcesDict.Add(TileType.Ore, r.ore);

        List<TileType> resourcesNeeded = new List<TileType>();
        resourcesExcess = new List<TileType>();
        for (TileType t = TileType.Wood; t <= TileType.Ore; t++)
            if (resourcesDict[t] > 0)
                resourcesNeeded.Add(t);
            else if (resourcesDict[t] < 0)
                resourcesExcess.Add(t);

        int excessAmount = resourcesExcess.Aggregate(0, (current, t) => (current - resourcesDict[t]));  // How many extra resources the AI has 

        int neededAmount = resourcesNeeded.Sum(t => resourcesDict[t]);  // How many resources the AI needs in total

        if (neededAmount == 0 || excessAmount == 0)
        {
            board.Next();
            return;
        }
            
        resourceNeeded = resourcesDict.Aggregate((x, y) => x.Value > y.Value ? x : y).Key; // The resource where the most is needed of 

        ratio = excessAmount / (double)neededAmount; //ratio between the needed resources and the owned ones
        if (ratio < 1)
        {
            board.Next();
            return;
        }

        resourcesAway = 1;
        MakeTrade();
    }

    //function to actually do the trade
    private void MakeTrade()
    {
        if (ratio < resourcesAway)
        {
            board.Next();
            return;
        }
        
        Dictionary<TileType, int> give = new Dictionary<TileType, int>();
        Dictionary<TileType, int> take = new Dictionary<TileType, int>();
        //initialize every possible tiletype with a value of 0 to avoid a key not found error
        foreach (TileType i in Enum.GetValues(typeof(TileType)))
        {
            give.Add(i ,0);
            take.Add(i, 0);
        }

        int amountNeeded = resourcesDict[resourceNeeded];
        int amountToGiveAway = amountNeeded * resourcesAway;
        take[resourceNeeded] = amountNeeded;
        for (TileType t = TileType.Wood; t <= TileType.Ore; t++)
        {
            if (amountToGiveAway <= 0)
                break;
            if (resourcesDict[t] < 0)
                if(amountToGiveAway >= -resourcesDict[t])
                {
                    give[t] = -resourcesDict[t];
                    amountToGiveAway += resourcesDict[t];
                }
                else
                {
                    give[t] = amountToGiveAway;
                    amountToGiveAway = 0;
                }
        }

        //these switches are to make sure the ai trades with a harbor or the bank if possible
        switch (resourcesAway)
        {
            case 2:
                foreach (HarborType ht in harbors.Where(ht => ht.ToString() == resourceNeeded.ToString()))
                {
                    board.ProcessBankTrade(give, take);
                    return;
                }
                board.PlayerTrade(give, take);
                break;
            case 3:
                foreach (HarborType ht in harbors.Where(ht => ht == HarborType.Three))
                {
                    board.ProcessBankTrade(give, take);
                    return;
                }
                board.PlayerTrade(give, take);
                break;
            case 4:
                board.ProcessBankTrade(give, take);
                break;
            case 1:
                board.PlayerTrade(give, take);
                break;
            default:
                board.Next();
                break;
        }
    }
    
    //function that is called when every player has chosen weither or not to accept a trade made by the ai
    public override void AcceptedTrade(List<Player> acceptedPlayers)
    {
        // If no one accepted the trade give more cards away to make the deal more attractive
        if (acceptedPlayers.Count == 0)
        {
            resourcesAway++;
            MakeTrade();
        }
        else
        {
            // If somebody dit accept the trade pick the one with the least victory points
            Player leastVictoryPoints = null;
            int vPoints = 11;
            foreach (Player p in acceptedPlayers.Where(p => p.victoryPoints < vPoints))
            {
                vPoints = p.victoryPoints;
                leastVictoryPoints = p;
            }
            
            board.ProcessPlayerTrade(leastVictoryPoints);

            Trading();
        }
    }

    
    //function for the AI to respond to a trade offer
    public override void ProposeTrade()
    {
        UpdateIncomeRate();
        
        if (RouteChanged() || !targetChosen)
            DetermineTarget();

        resources[TileType.Wool] = 0;
        
        ResourceSet r = costs[target.targetType].Remove(ResourceAmounts); 
        // The result is dictionary where a negative value means there are more resources than required and a positive one means more resources then required
        resourcesDict = new Dictionary<TileType, int>();
        resourcesDict.Add(TileType.Stone, r.stone);
        resourcesDict.Add(TileType.Wood, r.wood);
        resourcesDict.Add(TileType.Wool, r.wool);
        resourcesDict.Add(TileType.Wheat, r.wheat);
        resourcesDict.Add(TileType.Ore, r.ore);
        resourcesDict.Add(TileType.Empty, 0);
        resourcesDict.Add(TileType.Water, 0);

        bool enoughResources = true;
        bool usefullResources = false;
        foreach (TileType i in Enum.GetValues(typeof(TileType)))
        {
            if (board.playerAtTurn.proposedTradeGive[i] > 0 && resourcesDict[i] > 0)
                usefullResources = true;
            if (board.playerAtTurn.proposedTradeReceive[i] > 0 &&
                -resourcesDict[i] < board.playerAtTurn.proposedTradeReceive[i])
                enoughResources = false;
        }
        
        if(enoughResources && usefullResources)
            Debug.Log(playerName + " liked that offer");
        else
            Debug.Log(playerName + " did not like that offer");
        
        RespondToOffer(enoughResources && usefullResources);
    }

    public override void Development()
    {
        if (board.gameEnded)
            return;
        stackOverflowPreventer++;
        if (stackOverflowPreventer > 5)  // This means the AI has returned to this method too often; it should only happen a few times if the AI wants a new target in the same turn, but not so much
        // Although these stackoverflows have been fixed, we decided to keep this part of the code, in case it might prevent an error we have not yet discovered
        {
            Debug.Log("A STACKOVERFLOW HAS BEEN PREVENTED!");
            return;
        }
        if (RouteChanged() || !targetChosen)  // The board changes each turn. If the target the AI had in mind no longer works (or if it didn't have a target), it chooses a new one
            DetermineTarget();
        if (RobberNearby() && CountDevClass(DevelopmentCard.CardType.KNIGHT) > 0 && !board.devCardPlayed)  // If the robber is bothering the AI and the AI has a knight, it wants to use the knight to remove the robber
            ChosenCard(DevelopmentCard.CardType.KNIGHT);
        if (target.targetType == TargetType.none || board.gameEnded)  // gameEnded has to be checked in a lot of places; basically everywhere right after the AI can get victory points. Otherwise, the AI continues after winning
            return;
        if (!HasResources(costs[target.targetType]) && !board.devCardPlayed)  // Before trying to build, if the AI hasn't even got the resources to build the target, we can use some devcards to try and get these resources
        {
            if (CountDevClass(ProgressCard.Progress.MONOPOLY) > 0)
                ChosenCard(ProgressCard.Progress.MONOPOLY);
            else if (CountDevClass(ProgressCard.Progress.PLENTY) > 0)
                ChosenCard(ProgressCard.Progress.PLENTY);
        }
        if (!board.devCardPlayed)  // If no devcard has been played yet, the AI might as well play its roadbuilding card, because that is otherwise not required
        {
            if (CountDevClass(ProgressCard.Progress.ROADS) > 0)
            {
                ChosenCard(ProgressCard.Progress.ROADS);
                int saveGuard = 0;
                while (freeRoads > 0 && saveGuard < 5)
                {
                    BuildRoad(true);
                    if (board.gameEnded)
                        return;
                    saveGuard++;
                }
            }
            else if (CountDevClass(DevelopmentCard.CardType.KNIGHT) > 1)  // If the AI doesn't have the roads card, but it does have an extra knight (keeping one knight in reserve in case of robber), it plays the knight
            {
                ChosenCard(DevelopmentCard.CardType.KNIGHT);
                if (board.gameEnded)
                    return;
            }
        }
        if (GetTargetPossible())  // Now that the AI has done whatever it can to make building the target possible, it's time to actually build it, if it can
        {
            GetTarget();
            if (board.gameEnded)
                return;
        }
        else
        {
            if (!disableStreets && roadsLeft != 0 && HasResources(2, 2, 0, 0, 0))  // If the AI has to wait further until it can build the target, we might as well build a road in advance if we have the extra resources (perhaps that road even is required in order to build a settlement, otherwise the road is just a nice extra to spend superfluous resources on
            {
                BuildRoad(false);
                if (board.gameEnded)
                    return;
            }
            if (board.devCardStack.Count != 0 && resources[TileType.Wool] > costs[target.targetType].wool && resources[TileType.Wheat] > costs[target.targetType].wheat && resources[TileType.Ore] > costs[target.targetType].ore)  // If we've got enough resources to buy a devcard, even if we substract the resources required for the target, buy it
            {
                board.BuyDevCard();
                if (board.gameEnded)
                    return;
            }
        }
    }

    public override void EndTurn()
    {
        if (board.gameEnded)
            return;
        board.Next();
    }
    private bool GetTargetPossible()
    {
        switch (target.targetType)
        {
            case TargetType.settlement when settlementsLeft == 0 || !(target.location?.SettlementCanBeBuilt(this, true, true) ?? false):
                return false;
            case TargetType.city when citiesLeft == 0 || !(target.location?.CityCanBeBuilt(this) ?? false):
                return false;
            case TargetType.devcard when !HasResources(ResourceSet.devcardCost):
                return false;
        }
        return true;
    }

    private void GetTarget()  // For actually building/buying the target
    {
        switch (target.targetType)
        {
            case TargetType.settlement:
                target.location.BuySettlement(this);
                if (settlementsLeft == 0)
                    disableSettlements = true;
                Debug.Log(playerName + " has bought a settlement!");
                break;
            case TargetType.city:
                target.location.BuyCity(this);
                if (citiesLeft == 0)
                    disableCities = true;
                Debug.Log(playerName + " has bought a city!");
                break;
            case TargetType.devcard:
                board.BuyDevCard();
                Debug.Log(playerName + " has bought a devcard!");
                break;
        }
        Development();  // If the target has been obtained, the AI might be able to also obtain the new target within the same turn, so it reconsiders its options
    }

    private bool RobberNearby()
    {
        return board.tiles.Where(t => t.hasRobber).SelectMany(t => t.surroundingIntersections).Any(i => i.player == this);
    }

    private void DetermineTarget()  // Choosing targetType and resource the AI wants
    {
        // 'Choose what the AI wants'
        bool devCard = HasResources(ResourceSet.devcardCost) && ((maxCities - citiesLeft) < devCardsPlayed || citiesLeft == 0);  // A balancing feature: the AI might otherwise constantly choose development cards and never get a city.
        bool city = HasResources(ResourceSet.cityCost) && !disableCities;
        bool settlement = HasResources(ResourceSet.settlementCost) && !disableSettlements;

        TargetType targetType = TargetType.settlement;  // In case nothing is chosen below
        if (city)
        {
            targetType = TargetType.city;
        }
        else if (settlement)
        {
            targetType = TargetType.settlement;
        }
        else if (devCard)
        {
            targetType = TargetType.devcard;
        }
        else
        {
            // First calculate how much income there is for each required resource of each target type
            float scoreCity = (incomeRate[TileType.Ore] + incomeRate[TileType.Wheat]) / 5f;
            float scoreSettlement = (incomeRate[TileType.Stone] + incomeRate[TileType.Wood] +
                                     incomeRate[TileType.Wool] + incomeRate[TileType.Wheat]) / 4f;
            float scoreDevCard = (incomeRate[TileType.Ore] + incomeRate[TileType.Wool] + incomeRate[TileType.Wheat]) / 3f;

            if (settlementsLeft == 0 || disableSettlements) scoreSettlement = 0;
            if (citiesLeft == 0 || disableCities) scoreCity = 0;
            if (board.devCardStack.Count == 0) scoreDevCard = 0;

            // Then pick the one with the highest score
            if (scoreCity > scoreSettlement && scoreCity > scoreDevCard)
                targetType = TargetType.city;
            else if (scoreSettlement > scoreCity && scoreSettlement > scoreDevCard)
                targetType = TargetType.settlement;
            else if (scoreDevCard > scoreSettlement && scoreDevCard > scoreCity)
                targetType = TargetType.devcard;
            else targetType = TargetType.none;
        }

        Target determinedTarget = new Target(targetType, lowestResource);
        if (BannedTarget(determinedTarget))  // This is a huge peace of code, which basically lets the AI consider yet another choice time and again until it finally reaches a possiblity that is not banned
        // If the target is fine, the code below is not executed, and otherwise the method is designed to return as soon as it has found an option, so not the entire code is looped through every time
        {
            // Since so many possiblities have to be considered, a dictionary of all these resources with their income rates will be useful. One already exists, but a copy is created here, so that resources for which all targets are banned can be removed while looking for other options
            Dictionary<TileType, int> temporaryIncomes = new Dictionary<TileType, int>(incomeRate);
            for (int i = 0; i > temporaryIncomes.Count; i++)
            {
                TileType nextHighest = temporaryIncomes.Aggregate((x, y) => x.Value > y.Value ? x : y).Key;
                if (determinedTarget.targetType == TargetType.settlement)
                {
                    determinedTarget.targetType = TargetType.city;  // The same resource might be acquired through a city instead
                    if (!BannedTarget(determinedTarget))
                        return;
                    determinedTarget.targetType = TargetType.settlement;  // If that doesn't work, though, we should reset the type to its initial choice
                }
                else  // Now we do the same the other way around. In this case, the target is a city
                {
                    determinedTarget.targetType = TargetType.settlement;
                    if (!BannedTarget(determinedTarget))
                        return;
                    else
                        determinedTarget.targetType = TargetType.city;
                }
                // If the code hasn't returned by now, another resource must be chosen. The highest resources seems to be banned for both cases, so it is removed from our options
                temporaryIncomes.Remove(nextHighest);
                // Then the loop does the same, choosing a new highest value
            }
            if (target.targetType == TargetType.settlement)
                disableSettlements = true;
            else
                disableCities = true;
        }
        target = determinedTarget;
        if (target.targetType == TargetType.none)
            Debug.Log(playerName + " could not find a suitable target");
        else if (target.targetType == TargetType.devcard)
            Debug.Log(playerName + " wants a " + targetType);
        else
            Debug.Log(playerName + " wants a " + target.resource + " from a " + target.targetType);
        if (target.targetType != TargetType.none) FindTarget();
    }

    private bool BannedTarget(Target t)
    {
        return t.targetType != TargetType.devcard && targetOptions.Any(option =>
            option.Key.targetType == t.targetType && option.Key.resource == t.resource && option.Value);
    }

    private bool SatisfiesTarget(Intersection i)
    {
        switch (target.targetType)
        {
            case TargetType.settlement when i.building != BuildingType.Empty || !i.AdjacentSettlements(this):
            case TargetType.city when i.building != BuildingType.Settlement || i.player != this:
                return false;
        }

        return i.surroundingTiles.Any(t => t.type == target.resource);
    }

    private bool RouteChanged()
    {
        if (target.targetType == TargetType.none)  // This is not really a change of route, but if the target is none, a new one should be sought. It works just as well here as in any other part of the code, especially since this code is required anyway to prevent nullreferences.
            return true;
        if (target.targetType == TargetType.devcard)
            return board.devCardStack.Count == 0;
        if (!targetChosen)
            return false;
        if (routeStreets.Any(s => s.built && s.player != this))
            return true;
        return !target.location.BuildingCanBeBuilt();
    }

    // Actually binding a location to the wished TargetType and resource
    private void FindTarget()  // changeTarget should be false for checking without changing the actual target
    {
        switch (target.targetType)
        {
            case TargetType.settlement:
                streetDistances = new Dictionary<Street, int>();
                distance = 0;
                foreach (Street s in board.streets.Where(s => s.player == this))
                    streetDistances.Add(s, distance);
                CheckStreets();
                break;
            case TargetType.city:
                CheckSettlements();
                break;
            case TargetType.devcard:
                targetChosen = true;
                break;
        }
    }

    private void CheckStreets()
    {
        while (true)
        {
            bool checkAgain = true;
            List<Street> checkAdditions = new List<Street>();
            foreach (KeyValuePair<Street, int> entry in streetDistances)
            {
                foreach (Intersection i in entry.Key.intersections)
                {
                    if (SatisfiesTarget(i))
                    {
                        target.location = i;
                        checkAgain = false;
                        targetChosen = true;
                        Debug.Log(playerName + " is now targeting " + i);
                    }
                    else
                        checkAdditions.AddRange(i.streets.Where(nextS => nextS != null && !streetDistances.ContainsKey(nextS) && !nextS.built));
                }
            }

            if (checkAgain)
            {
                distance++;
                foreach (Street s2Add in checkAdditions.Where(s2Add => !streetDistances.ContainsKey(s2Add)))
                    streetDistances.Add(s2Add, distance);
                if (distance < 5)
                    continue;
                // If the code reaches this point, the AI apparently can't find any corresponding locations, and the target should be banned
                targetOptions[new Target(target.targetType, target.resource)] = true;
                // Then a new target is determined
                DetermineTarget();
            }
            else
                foreach (Street routeStart in target.location.streets)
                {
                    if (streetDistances == null || routeStart == null)
                        return;
                    if (streetDistances.ContainsKey(routeStart) && routeStart.player != this)  // Out of all the streets that surround the targeted intersection, any street that belongs to the web of roads should be fine as a starting point for the quickest route. If all required roads have already been built, though (i.e. if this very road also belongs to the player), it shouldn't be added to the route
                    {
                        routeStreets.Add(routeStart);
                        distanceCountdown = streetDistances[routeStart];
                        RouteConstruction(routeStart);
                        return;
                    }
                }
            break;
        }
    }

    private void RouteConstruction(Street startStreet)
    {
        distanceCountdown--;
        foreach (Intersection i in startStreet.intersections)
            foreach (Street s in i.streets.Where(s => s != null && streetDistances.ContainsKey(s) && streetDistances[s] == distanceCountdown))
            {
                if (s.player != this)
                    routeStreets.Add(s);
                RouteConstruction(s);
                return;
            }
    }

    private void BuildRoad(bool free)
    {
        if (!free && !HasResources(ResourceSet.streetCost)) return;
        foreach (Street s in board.streets.Where(s => routeStreets.Contains(s) && s.StreetCanBeBuilt(this, !free, true)))
        {
            s.BuyStreet(this);
            if (!free)
                Development();
            return;
        }
        BuildRandomRoad(free);
    }

    private void BuildRandomRoad(bool free)
    {
        foreach (Street s in board.streets.Where(s => s.StreetCanBeBuilt(this, !free, true)))
        {
            s.BuyStreet(this);
            if (!free)
                Development();
            return;
        }
        disableStreets = true;
        Debug.Log("Streets have been disabled for " + playerName);
    }

    private void CheckSettlements()
    {
        foreach (var i in board.intersections.Where(i => i.player == this && SatisfiesTarget(i)))
        {
            target.location = i;
            targetChosen = true;
            return;
        }
        // If the code reaches this point, the AI apparently can't find any corresponding locations, and the target should be banned
        targetOptions[new Target(target.targetType, target.resource)] = true;
        // Then a new target is determined
        DetermineTarget();
    }

    public override void Robber()
    {
        // First we find the players with the most victorypoints, this will be our target
        int mostVictoryPoints = 0;
        for (int i = 0; i < 4; i++)
        {
            if (board.players[i].victoryPoints >= mostVictoryPoints)
            {
                mostVictoryPoints = board.players[i].victoryPoints;
                targetPlayer = board.players[i];
            }
        }
        // Then we find a tile where the player has the highest probability of getting a resource combined with the amount of settlement / city around the tile
        // We could block the player a lot more if we pick a tile were he would normally get 3 resources out of instead of 1 for example
        Tile bestTile = null;
        int bestTileScore = 0;
        foreach (Tile t in board.tiles)
        {
            if (t.type == TileType.Water)
                continue;

            int tileScore = 0;
            for (int i = 0; i < 6; i++)
            {
                if (t.surroundingIntersections[i].player == targetPlayer &&
                    t.surroundingIntersections[i].building == BuildingType.Settlement)
                    tileScore ++;
                if (t.surroundingIntersections[i].player == targetPlayer &&
                    t.surroundingIntersections[i].building == BuildingType.City)
                    tileScore += 2;
                if (t.surroundingIntersections[i].player == this &&
                    t.surroundingIntersections[i].Built)
                {
                    tileScore = 0;
                    break;
                }
            }

            tileScore *= probabilities[t.number];

            foreach (Intersection i in t.surroundingIntersections)
                if (i.player == this) tileScore = 0;

            if (tileScore > bestTileScore)
            {
                bestTileScore = tileScore;
                bestTile = t;
            }
        }

        if (bestTile == null)  // This is possible if the ai chose a tile which it is one as the best tile
        {
            List<Tile> suitableTiles = new List<Tile>();
            // Get all tiles that dont have the player on it
            foreach (Tile t in board.tiles)
            {
                if (t.type == TileType.Water || t.type == TileType.Empty)
                    continue;

                bool noOwnedIntersections = true;
                foreach (Intersection i in t.surroundingIntersections)
                    if (i.Built && i.player == this)
                        noOwnedIntersections = false;
                if(noOwnedIntersections)
                    suitableTiles.Add(t);
            }
            
            // Then pick a random one
            bestTile = suitableTiles[Random.Range(0, suitableTiles.Count)];
            // ...aaand make sure the AI is not going to be looking for the targeted player anymore, because that player might very well not be on the random tile
            targetPlayer = null;
        }
        bestTile.OnMouseDown();
    }

    public override void ChooseRobberVictim(Tile tile)
    {
        board.phase = Phase.CHOOSEVICTIM;
        foreach (Intersection i in tile.surroundingIntersections)
            if (i.player == targetPlayer || targetPlayer == null)
            {
                i.Confirmed();
                return;
            }
        board.ReturnFromRobber();  // This is called if there are no players surrounding the tile. In this case, the AI won't steal a card, but return straight away, just like a player.
    }

    // This is just a empty function because it is not useful for the rule based ai
    public override void NotEnoughResources() {}

    public override void DevActionPanel(int panelNumber)
    {
        if (panelNumber == 0)  // Monopoly is played
        {
            monopolyEnabled = !monopolyEnabled;
            if (monopolyEnabled)
                board.MonopolyExecute(ResourceNeeded());
        }
        else  // Plenty is played
        {
            // ...... that is, unless discarding is happening
            if (board.plenty.takeCards)
            {
                discardEnabled = !discardEnabled;
                if (discardEnabled)
                {
                    board.plenty.resourcesLeft = board.plenty.amountToPick;
                    DiscardUnvaluable();
                }
            }
            else
            {
                plentyEnabled = !plentyEnabled;
                if (plentyEnabled)
                    switch (ResourceNeeded())
                    {
                        case TileType.Wood:
                            board.PlentyExecute(2, 0, 0, 0, 0);
                            break;
                        case TileType.Stone:
                            board.PlentyExecute(0, 2, 0, 0, 0);
                            break;
                        case TileType.Wool:
                            board.PlentyExecute(0, 0, 2, 0, 0);
                            break;
                        case TileType.Wheat:
                            board.PlentyExecute(0, 0, 0, 2, 0);
                            break;
                        case TileType.Ore:
                            board.PlentyExecute(0, 0, 0, 0, 2);
                            break;
                    }
            }
        }
    }

    private void DiscardUnvaluable()
    {
        while (true)
        {
            if (board.plenty.resourcesLeft == 0)  // This method is on loop, and only stops if there are no resources left to discard
            {
                board.PlentyExecute(0, 0, 0, 0, 0);  // 0 values because the AI dumps resources one by one and then reconsiders
                return;
            }

            ResourceSet r = costs[target.targetType].Remove(ResourceAmounts).Negative;  // The result is a set of the resources the AI has too much for their building plan
            IDictionary<TileType, int> d = new Dictionary<TileType, int>();
            d.Add(TileType.Stone, r.stone);
            d.Add(TileType.Wood, r.wood);
            d.Add(TileType.Wool, r.wool);
            d.Add(TileType.Wheat, r.wheat);
            d.Add(TileType.Ore, r.ore);
            TileType highest = d.Aggregate((x, y) => x.Value > y.Value ? x : y).Key;  // Gets the highest value, which is the one of which the AI has too much
            if (resources[highest] == 0)  // If the AI can no longer discard resources it doesn't need, it will be forced to dump resources it does need, and it will take on a different tactic
            {
                DiscardValuable();
                break;
            }
            // Otherwise, it can repeat this method
            resources[highest]--;
            board.plenty.resourcesLeft--;
        }
    }

    private void DiscardValuable()
    {
        while (true)
        {
            if (board.plenty.resourcesLeft == 0)  // This method is on loop, and only stops if there are no resources left to discard
            {
                board.PlentyExecute(0, 0, 0, 0, 0);  // 0 values because the AI dumps resources one by one and then reconsiders
                return;
            }

            TileType highest = resources.Aggregate((x, y) => x.Value > y.Value ? x : y).Key;  // Gets the highest value, which is the resource it has the most
            resources[highest]--;
            board.plenty.resourcesLeft--;
        }
    }
}
