using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//[ExecuteInEditMode]
public class PartialBillboard : MonoBehaviour
{
    private Transform theCamera;
    
    private void Awake()
    {
        theCamera = GameObject.Find("Main Camera").transform;
    }

    private void LateUpdate()
    {
        transform.LookAt(theCamera.transform.position - 40 * new Vector3(theCamera.transform.forward.x, 0, theCamera.transform.forward.z).normalized);
        transform.Rotate(transform.localRotation.eulerAngles.x + 90, 180, 0);
    }
}
