using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Die : MonoBehaviour
{
    [HideInInspector] public Rigidbody rb;
    private DieSideChecker _dsc;
    public int SideValue;
    public int PreviousSideValue;

    // Physics values, can be changed in inspector  // Well they are private and constant so they can't
    private const float maxTorque = 2500;
    private const float minTorque = -2500;
    private const float maxForce = 3200;
    private const float minForce = 2800;

    private void Awake()
    {
        rb = gameObject.GetComponent<Rigidbody>();
        _dsc = GameObject.Find("Table Brown").GetComponent<DieSideChecker>();
    }

    private void Start()
    {
        // When it is instantiated into the Scene
        _dsc.diceActive.Add(gameObject);
        _dsc.dice.Add(gameObject.GetComponent<Die>());
        rb.AddTorque(RandomTorque(), RandomTorque(), RandomTorque());
        rb.AddForce(new Vector3(0, 0, Random.Range(minForce, maxForce)));
    }

    private static float RandomTorque()
    {
        return Random.Range(minTorque, maxTorque);
    }
}
