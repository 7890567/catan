using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Object = UnityEngine.Object;

public abstract class DevelopmentCard
{
    public Board board;
    public enum CardType { KNIGHT, VICTORY, PROGRESS }
    public CardType cardtype;

    public virtual void PlayCard()
    {
        board = Object.FindObjectOfType<Board>();
        board.playerAtTurn.Panel(2);
        board.playerAtTurn.ResetOpenIcon();
        board.playerAtTurn.devCards.Remove(this);
        board.devCardPlayed = true;
        board.playerAtTurn.devCardsPlayed++;
        board.playerAtTurn.UpdateDevCards();
    }
}

public class KnightCard : DevelopmentCard
{
    public KnightCard()
    {
        cardtype = CardType.KNIGHT;
    }
    public override void PlayCard()
    {
        base.PlayCard();
        board.playerAtTurn.knightsUsed++;
        board.UpdateMostKnights();
        board.returnPhase = Phase.BUILDING;
        board.RobberPhase();
        board.playerAtTurn.Robber(); //For AI
    }
}

public class VictoryCard : DevelopmentCard
{
    public enum Structure { UNIVERSITY, MARKET, GREAT_HALL, CHAPEL, LIBRARY }
    public Structure structure;

    // The structure cards do the same, so they don't need a separate class
    // Consequently, they don't need their own constructor methods: the structure type is given as a parameter
    public VictoryCard(Structure str)
    {
        cardtype = CardType.VICTORY;
        structure = str;
    }

    public override void PlayCard()
    {
        // Removed due to different choice of game rules, but should we want non-standard rules, the code is here
        /*base.PlayCard();
        board.playerAtTurn.AddVictoryPoints(1);
        board.playerAtTurn.Popup("You got one victory point!", "Victory Card");*/
    }
}

public abstract class ProgressCard : DevelopmentCard
{
    public enum Progress { ROADS, PLENTY, MONOPOLY }
    public Progress progress;

    protected ProgressCard()
    {
        cardtype = CardType.PROGRESS;
    }
    public override void PlayCard()
    {
        base.PlayCard();
        board.devCardStack.Add(this);
    }
}

public class RoadsCard : ProgressCard
{
    public RoadsCard() : base()
    {
        progress = Progress.ROADS;
    }
    public override void PlayCard()
    {
        Player p = board.playerAtTurn;
        if (p.roadsLeft > 0)
        {
            base.PlayCard();
            board.returnPhase = Phase.BUILDING;
            board.playerAtTurn.AddFreeRoads(Math.Min(2, p.roadsLeft));
            board.playerAtTurn.ShowPopupInstructions("2 Roads left");
            board.FreeRoads();
        }
        else
            p.Popup("You don't have any roads left.", "Building");
    }
}

public class PlentyCard : ProgressCard
{
    public PlentyCard() : base()
    {
        progress = Progress.PLENTY;
    }
    public override void PlayCard()
    {
        base.PlayCard();
        board.Plenty(false, 2);
    }
}

public class MonopolyCard : ProgressCard
{
    public MonopolyCard() : base()
    {
        progress = Progress.MONOPOLY;
    }
    public override void PlayCard()
    {
        base.PlayCard();
        board.playerAtTurn.DevActionPanel(0);
    }
}