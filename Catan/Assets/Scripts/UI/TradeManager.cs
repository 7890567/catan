using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using TMPro;

public class TradeManager : Selectable
{
    // Resources Player will (Give | Receive) from other players
    public IDictionary<TileType, int> playerGive = new Dictionary<TileType, int>();
    public IDictionary<TileType, int> playerReceive = new Dictionary<TileType, int>();

    // Resources Player will (Give | Receive) from bank
    public IDictionary<TileType, int> bankGive = new Dictionary<TileType, int>();
    public IDictionary<TileType, int> bankReceive = new Dictionary<TileType, int>();

    // List of used (Input)Text Fields
    public List<TMP_InputField> iFields = new List<TMP_InputField>();

    bool playerTrade = false;
    bool bankTrade = false;

    bool clicked = false;

    protected override void Awake()
    {
        // Get all references
        board = FindObjectOfType<Board>();
        confirmButton = GameObject.Find("Button - Confirm");
        confirmScript = confirmButton.GetComponent<Confirm>();

        selectable = this;

        foreach (TileType i in Enum.GetValues(typeof(TileType)))
        {
            playerGive.Add(i, 0);
            playerReceive.Add(i, 0);
            bankGive.Add(i, 0);
            bankReceive.Add(i, 0);
        }
    }

    public override void Confirmed()
    {
        if (playerTrade)
            board.PlayerTrade(playerGive, playerReceive);
        if (bankTrade)
        {
            board.ProcessBankTrade(bankGive, bankReceive);
            board.playerAtTurn.UpdateResourcesUI();
        }

        board.playerAtTurn.ResetOpenIcon();
        ResetPanel();
    }

    public void ResetPanel()
    {
        // Reset all resources
        foreach (TileType i in Enum.GetValues(typeof(TileType)))
        {
            playerGive[i] = 0;
            playerReceive[i] = 0;
            bankGive[i] = 0;
            bankReceive[i] = 0;
        }

        // Reset all (Input)Text Fields
        foreach (TMP_InputField iField in iFields)
            iField.text = "";

        clicked = false;
        gameObject.SetActive(false);
    }

    private void Update()
    {
        // Count relative resource amounts of each dictionary
        int nPGive = CountResources(playerGive, false);
        int nPReceive = CountResources(playerReceive, false);

        int nBGive = CountResources(bankGive, true);
        int nBReceive = CountResources(bankReceive, false);

        bool hasResources = CheckHasResources();

        // See if a Player trade can be proposed
        if (nPGive != 0 || nPReceive != 0)
            playerTrade = true;
        else
            playerTrade = false;

        // See if a Bank trade can be processed
        if (nBGive != 0 && nBGive == nBReceive)
            bankTrade = true;
        else
            bankTrade = false;

        // Activates | Deactivates Confirm Button according to whether a trade can be done or not
        if (((playerTrade && nBGive == 0 && nBReceive == 0) || bankTrade) && hasResources && !clicked)
        {
            Clicked();
            clicked = true;
        }
            
        else if (!(((playerTrade && nBGive == 0 && nBReceive == 0) || bankTrade) || !hasResources) && clicked)
            Deactivate();
    }

    private int CountResources(IDictionary<TileType, int> resourceList, bool specialCase)
    {
        // Counts relative resource amount of a single dictionary
        int result = 0;
        foreach (TileType tileType in Enum.GetValues(typeof(TileType)))
        {
            int resourceAmount = resourceList[tileType];
            // If the player gives resources to the bank: does he have a special Harbor?
            if (specialCase)
            {
                if (board.playerAtTurn.harbors.Contains((HarborType)(int)(tileType)))
                    resourceAmount /= 2;
                else if (board.playerAtTurn.harbors.Contains(HarborType.Three))
                    resourceAmount /= 3;
                else
                    resourceAmount /= 4;
            }
            result += resourceAmount;
        }
            
        return result;
    }

    private bool CheckHasResources()
    {
        int[] totalResourceList = new int[5];

        for (int i = 0; i < 5; i++)
        {
            // Disregarding Empty & Water (i + 2)
            totalResourceList[i] += playerGive[(TileType)(i + 2)];
            totalResourceList[i] += bankGive[(TileType)(i + 2)];
        }

        return board.playerAtTurn.HasResources(totalResourceList[0], totalResourceList[1], totalResourceList[2], totalResourceList[3], totalResourceList[4]);
    }

    private void Deactivate()
    {
        confirmButton.SetActive(false);
        confirmScript.CurrentSelectable = null;
        clicked = false;
    }
}
