using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class Plenty : MonoBehaviour
{
    Board board;
    public TMP_Text title;
    public TMP_InputField[] iFields = new TMP_InputField[5];  // Wood, Stone, Wool, Wheat, Ore
    public TMP_Text resourcesText;
    public int amountToPick;
    public int resourcesLeft;
    public bool takeCards;

    public Dictionary<TileType, int> resources = new Dictionary<TileType, int>()
    {
        { TileType.Wood, 0 },
        { TileType.Stone, 0 },
        { TileType.Wool, 0 },
        { TileType.Wheat, 0 },
        { TileType.Ore, 0 }
    };
    public int ResourceAmount => resources[TileType.Wood] + resources[TileType.Stone] + resources[TileType.Wool] + resources[TileType.Wheat] + resources[TileType.Ore];

    void Awake()
    {
        board = FindObjectOfType<Board>();
        SetZero();
    }

    public void SetZero()
    {
        foreach (TMP_InputField i in iFields)
            i.text = "0";
        UpdateResourcesLeft();
    }

    public void UpdateResourcesLeft()
    {
        resourcesText.text = resourcesLeft + " resources left";
    }
    public void EditResourcesLeft(int changeAmount)
    {
        resourcesLeft += changeAmount;
        resourcesText.text = resourcesLeft + " resources left";
    }

    public void UpdateTexts()
    {
        if (takeCards)
            title.text = "Discard";
        else
            title.text = "Year of Plenty";
        UpdateResourcesLeft();
    }
}
