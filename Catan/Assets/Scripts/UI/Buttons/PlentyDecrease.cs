using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class PlentyDecrease : ButtonTemplate
{
    public TileType resource;
    public TMP_InputField iField;
    Plenty plenty;

    protected override void Awake()
    {
        base.Awake();
        plenty = GameObject.Find("Panel - DevCard - Year Of Plenty").GetComponent<Plenty>();
    }

    protected override void Clicked()
    {
        if (plenty.resources[resource] != 0)
        {
            plenty.resources[resource]--;
            iField.text = plenty.resources[resource].ToString();
            plenty.EditResourcesLeft(1);
        }
    }
}
