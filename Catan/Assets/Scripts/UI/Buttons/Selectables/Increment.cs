using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using TMPro;

public class Increment : TradeProposal
{
    
    protected override void Clicked()  // Adds resource(s) to (Input)Text Field
    {
        if (isGive && isPlayers && board.playerAtTurn.resources[resource] >= (tradeManager.playerGive[resource] + tradeManager.bankGive[resource] + 1))
        {
            tradeManager.playerGive[resource] += 1; 
            iField.text = tradeManager.playerGive[resource].ToString();
            AddIField();
        }           

        else if(!isGive && isPlayers)
        {
            tradeManager.playerReceive[resource] += 1;
            iField.text = tradeManager.playerReceive[resource].ToString();
            AddIField();
        }           

        // bankGive needs to take the harbor into account
        else if (isGive && !isPlayers && board.playerAtTurn.resources[resource] >= (tradeManager.playerGive[resource] + tradeManager.bankGive[resource] + factor))
        {
            tradeManager.bankGive[resource] += factor;
            iField.text = tradeManager.bankGive[resource].ToString();
            AddIField();
        }           

        else if (!isGive && !isPlayers)
        {
            tradeManager.bankReceive[resource] += 1;
            iField.text = tradeManager.bankReceive[resource].ToString();
            AddIField();
        }       
    }

    private void AddIField()  // For clearing purposes
    {
        if (!tradeManager.iFields.Contains(iField))
            tradeManager.iFields.Add(iField);
    }
}
