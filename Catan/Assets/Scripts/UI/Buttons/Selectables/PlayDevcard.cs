using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayDevcard : Selectable
{
    public GameObject devPanel;

    public int cardOption;  // Because different cards require different enums, but not all of them do, I can't always asign the enums. So I gave the cards a number, same order as in devPanel
    private DevelopmentCard developmentCard;
    private ProgressCard progressCard;
    private VictoryCard victoryCard;

    protected override void Clicked()
    {
        if (!board.playerAtTurn.ui.confirmButton.activeInHierarchy)
        {
            StartCoroutine(confirmScript.ShowConfirmButton());
            confirmScript.CurrentSelectable = selectable;
        }        
    }

    public override void Confirmed()
    {
        switch (cardOption)
        {
            // Victory cards are no longer used this way, but they are still in the code, in case we need it later
            case 0:
                // board.playerAtTurn.ChosenCard(VictoryCard.Structure.CHAPEL);
                break;
            case 1:
                // board.playerAtTurn.ChosenCard(VictoryCard.Structure.GREAT_HALL);
                break;
            case 2:
                // board.playerAtTurn.ChosenCard(VictoryCard.Structure.LIBRARY);
                break;
            case 3:
                //board.playerAtTurn.ChosenCard(VictoryCard.Structure.MARKET);
                break;
            case 4:
                // board.playerAtTurn.ChosenCard(VictoryCard.Structure.UNIVERSITY);
                break;
            case 5:
                board.playerAtTurn.ChosenCard(DevelopmentCard.CardType.KNIGHT);
                break;
            case 6:
                board.playerAtTurn.ChosenCard(ProgressCard.Progress.MONOPOLY);
                break;
            case 7:
                board.playerAtTurn.ChosenCard(ProgressCard.Progress.PLENTY);
                break;
            case 8:
                board.playerAtTurn.ChosenCard(ProgressCard.Progress.ROADS);
                break;
            case 9: // The buy option
                board.BuyDevCard();
                break;
        }
    }
}
