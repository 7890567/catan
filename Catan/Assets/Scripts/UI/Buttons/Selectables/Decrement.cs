using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using TMPro;

public class Decrement : TradeProposal
{
    
    protected override void Clicked()  // Removes resource(s) from (Input)Text Field
    {
        // Making sure not to go into negative
        if (isGive && isPlayers && (tradeManager.playerGive[resource] - 1) >= 0)
        {
            tradeManager.playerGive[resource] -= 1;
            if (tradeManager.playerGive[resource] == 0)
            {
                iField.text = "";
                RemoveIField();
            }              
            else
                iField.text = tradeManager.playerGive[resource].ToString();

        }           

        else if (!isGive && isPlayers && (tradeManager.playerReceive[resource] - 1) >= 0)
        {
            tradeManager.playerReceive[resource] -= 1;
            if (tradeManager.playerReceive[resource] == 0)
            {
                iField.text = "";
                RemoveIField();
            }                
            else
                iField.text = tradeManager.playerReceive[resource].ToString();
        }

        // bankGive needs to take the harbor into account
        else if (isGive && !isPlayers && (tradeManager.bankGive[resource] - factor) >= 0)
        {
            tradeManager.bankGive[resource] -= factor;
            if (tradeManager.bankGive[resource] == 0)
            {
                iField.text = "";
                RemoveIField();
            }               
            else
                iField.text = tradeManager.bankGive[resource].ToString();
        }
            

        else if (!isGive && !isPlayers && (tradeManager.bankReceive[resource] - 1) >= 0)
        {
            tradeManager.bankReceive[resource] -= 1;
            if (tradeManager.bankReceive[resource] == 0)
            {
                iField.text = "";
                RemoveIField();
            }               
            else
                iField.text = tradeManager.bankReceive[resource].ToString();
        }          
    }

    private void RemoveIField()  // For clearing purposes
    {
        tradeManager.iFields.Remove(iField);
    }
}
