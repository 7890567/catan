using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

// Used on Buttons that need comms with the Confirm Button when clicked on
public abstract class Selectable : ButtonTemplate
{
    protected GameObject confirmButton;
    protected Confirm confirmScript;
    protected Selectable selectable; 

    protected override void Awake()
    {
        base.Awake();
        confirmButton = GameObject.Find("Button - Confirm");
        confirmScript = confirmButton.GetComponent<Confirm>();
        selectable = this;
    }


    protected override void Clicked() // Activates the Confirm Button and informs that he should be the recipient
    {
        StartCoroutine(confirmScript.ShowConfirmButton());
        confirmScript.CurrentSelectable = selectable;
    }


    public abstract void Confirmed(); // Called when clicked on the Confirm Button
}
