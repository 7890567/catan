using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Monopoly : Selectable
{
    public GameObject monopolyPanel;

    public TileType resource;

    protected override void Awake()
    {
        base.Awake();
        selectable = gameObject.GetComponent<Monopoly>();
    }

    protected override void Clicked()
    {
        if (!board.playerAtTurn.ui.confirmButton.activeInHierarchy)
        {
            StartCoroutine(confirmScript.ShowConfirmButton());
            confirmScript.CurrentSelectable = selectable;
        }
    }

    public override void Confirmed()
    {
        board.MonopolyExecute(resource);
    }
}
