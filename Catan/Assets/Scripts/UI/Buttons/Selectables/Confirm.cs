using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class Confirm : ButtonTemplate
{
    private Selectable currentSelectable;  // The script the Confirm Button calls when pressed

    protected void Start()
    {
        gameObject.SetActive(false);
    }

    public Selectable CurrentSelectable
    {
        set { currentSelectable = value; }
    }

    public IEnumerator ShowConfirmButton()
    {
        yield return board.ui.wfs;
        gameObject.SetActive(true);
    }

    protected override void Clicked()  // Lets the Selectable know it is clicked on
    {
        gameObject.SetActive(false);
        currentSelectable.Confirmed();
        currentSelectable = null;
    }
}
