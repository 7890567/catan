using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public abstract class TradeProposal : ButtonTemplate
{
    protected TradeManager tradeManager;
    public TileType resource;

    public TileType Resource
    {
        get { return resource; }
    }

    public void Update() // Check for harbors
    {
        if (board.playerAtTurn.harbors.Contains((HarborType)(int)(resource)))
            factor = 2;
        else if (board.playerAtTurn.harbors.Contains(HarborType.Three))
            factor = 3;
    }

    // The Input Field that shows the player the selected resource amount
    public TMP_InputField iField;

    protected const string give = "Give", take = "Take", players = "Players", bank = "Bank";

    // Purpose of the Button
    protected bool isGive;
    protected bool isPlayers;

    // Factor of harbor
    public int factor = 4;
    
    public int Factor
    {
        get { return factor; }
        set { factor = value; }
    }

    protected override void Awake()
    {
        base.Awake();
        tradeManager = GameObject.Find("Panel - Phase - Trading").GetComponent<TradeManager>();

        // Checking in the Scene what the purpose of the button is
        Transform tChild = gameObject.transform;
        while (tChild != null && tChild.tag != players && tChild.tag != bank)
        {
            if (tChild.parent.tag == give)
                isGive = true;
            if (tChild.parent.tag == take)
                isGive = false ;
            if (tChild.parent.tag == players)
                isPlayers = true;
            if (tChild.parent.tag == bank)
                isPlayers = false;
            tChild = tChild.parent.transform;
        }
    }
}
