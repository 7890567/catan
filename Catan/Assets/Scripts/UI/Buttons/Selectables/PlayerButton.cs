using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerButton : ButtonTemplate  // For trade confirmation window
{
    public Player player;

    protected override void Clicked()
    {
        if (board.playerAtTurn.ui.pm.GetComponent<ProposalManager>().playerList[player] && !board.playerAtTurn.ui.confirmButton.activeInHierarchy)
            board.playerAtTurn.ui.pm.GetComponent<ProposalManager>().PlayerClicked(player);
    }
}
