using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OpenCardWindow : ButtonTemplate  // Opening either trade or dev. card window
{
    public GameObject tradeCardPanel;
    public GameObject devCardPanel;

    public Sprite[] arrows = new Sprite[2];
    public Image arrow;

    private bool isOpened = false;

    public bool IsOpened
    {
        set { isOpened = value; }
    }

    protected override void Clicked()  // Which panel is opened, depends on the phase
    {
        if (board.phase == Phase.TRADING && !board.turnBlock && Board.CheckEmpty())
        {
            if (isOpened)
                ResetConfirm();
            else
                board.ui.tm.ResetPanel();
            UpdateIcon();
            board.playerAtTurn.Panel(1);   
        }
            
        if (board.phase == Phase.BUILDING && !board.turnBlock)
        {
            if (isOpened)
                ResetConfirm();
            UpdateIcon();
            board.playerAtTurn.UpdateDevCards();
            board.playerAtTurn.Panel(2);            
        }
    }

    public void UpdateIcon()  // Updates icon to 'binary' equivalent, depending on whether the card window is opened or not
    {
        isOpened = !isOpened;
        if (!isOpened)
            arrow.sprite = arrows[0];      
        else
            arrow.sprite = arrows[1];
    }

    public void ResetConfirm()
    {
        board.ui.confirmButton.GetComponent<Confirm>().CurrentSelectable = null;
        board.ui.confirmButton.SetActive(false);
    }
}
