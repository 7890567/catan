using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;


public abstract class ButtonTemplate : MonoBehaviour  // Basic template for UI_Buttons with standard methods
{
    protected Button button;
    protected Board board;

    protected virtual void Awake()
    {
        button = gameObject.GetComponent<Button>();
        board = FindObjectOfType<Board>();
        button.onClick.AddListener(Clicked);
    }

    protected abstract void Clicked();  // Called when button is clicked
}
