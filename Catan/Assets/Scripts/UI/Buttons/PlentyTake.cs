using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using TMPro;

public class PlentyTake : ButtonTemplate
{
    Plenty plenty;
    protected override void Awake()
    {
        base.Awake();
        plenty = GameObject.Find("Panel - DevCard - Year Of Plenty").GetComponent<Plenty>();
    }

    protected override void Clicked()
    {
        if (plenty.resourcesLeft > 0)
            if (plenty.takeCards)
                board.discardingPlayer.Popup("You've still got resources left to choose!", "Discard");
            else
                board.playerAtTurn.Popup("You've still got resources left to choose!", "Year of Plenty");
        else
        {
            if (plenty.takeCards)
                board.PlentyExecute(-plenty.resources[TileType.Wood], -plenty.resources[TileType.Stone], -plenty.resources[TileType.Wool], -plenty.resources[TileType.Wheat], -plenty.resources[TileType.Ore]);
            else
                board.PlentyExecute(plenty.resources[TileType.Wood], plenty.resources[TileType.Stone], plenty.resources[TileType.Wool], plenty.resources[TileType.Wheat], plenty.resources[TileType.Ore]);
            plenty.resources[TileType.Wood] = plenty.resources[TileType.Stone] = plenty.resources[TileType.Wool] = plenty.resources[TileType.Wheat] = plenty.resources[TileType.Ore] = 0;
            plenty.SetZero();
        }
    }
}
