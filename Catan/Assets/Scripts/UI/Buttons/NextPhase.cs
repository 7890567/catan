using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class NextPhase : ButtonTemplate
{
    protected override void Clicked()
    {
        // Button can only be clicked when in TRADING phase || BUILDING phase and when there is no pending trade proposal
        if ((board.phase == Phase.TRADING || board.phase == Phase.BUILDING) && !board.turnBlock && Board.CheckEmpty())
            board.Next();       
    }
}
