using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProposalConfirmation : ButtonTemplate
{
    // Is the Button 'Confirm' or 'Decline' ?
    public bool confirm;

    public ProposalManager pm;
    [HideInInspector] public Player player;

    protected override void Clicked()
    {
        if (!board.playerAtTurn.ui.popupPanel.activeInHierarchy)
        {
            if (confirm && !CheckHasResources())
                player.NotEnoughResources();
            else
            {
                pm.playerList[player] = confirm;
                player = null;
                board.ui.tradeProposal.SetActive(false);
                board.ProposePlayerTrade();
            }          
        }     
    }

    private bool CheckHasResources()
    {
        int[] totalResourceList = new int[5];

        for (int i = 0; i < 5; i++)
            // Disregarding Empty & Water (i + 2)
            totalResourceList[i] += board.playerAtTurn.proposedTradeReceive[(TileType)(i + 2)];

        return player.HasResources(totalResourceList[0], totalResourceList[1], totalResourceList[2], totalResourceList[3], totalResourceList[4]);
    }
}
