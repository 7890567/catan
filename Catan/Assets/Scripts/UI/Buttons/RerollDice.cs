using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RerollDice : ButtonTemplate
{
    protected override void Clicked()
    {
        gameObject.SetActive(false);
        board.playerAtTurn.ui.dsc.ClearDie(false);
        board.playerAtTurn.ThrowDice();
    }
}
