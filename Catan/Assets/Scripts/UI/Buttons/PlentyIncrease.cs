using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class PlentyIncrease : ButtonTemplate
{
    public TileType resource;
    public TMP_InputField iField;
    Plenty plenty;

    protected override void Awake()
    {
        base.Awake();
        plenty = GameObject.Find("Panel - DevCard - Year Of Plenty").GetComponent<Plenty>();
    }

    protected override void Clicked()
    {
        if (plenty.ResourceAmount != plenty.amountToPick && (!plenty.takeCards || board.discardingPlayer.resources[resource] > plenty.resources[resource]))
        {
            plenty.resources[resource]++;
            iField.text = plenty.resources[resource].ToString();
            plenty.EditResourcesLeft(-1);
        }

    }
}
