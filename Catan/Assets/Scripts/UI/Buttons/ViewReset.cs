using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class ViewReset : ButtonTemplate
{
    public GameObject cam;
    
    private Vector3 camPos;  // Standard camPos
    public float camPosRadius;  // Distance relative to center
    public float camPosHeight;  // Height relative to center

    private Vector3 camRot;  // Standard camRot

    protected override void Awake()
    {
        base.Awake();

        camPos = cam.transform.position;
        // - 0 for flexibility
        camPosRadius = 0 - camPos.z; 
        camPosHeight = camPos.y - 0; 

        camRot = cam.transform.eulerAngles;
    }

    protected override void Clicked()
    {
        float angleOld;
        // Since eulerAngles doesn't take account of negative angles; has to be recalculated
        if (cam.transform.eulerAngles.y > 180)
            angleOld = cam.transform.eulerAngles.y - 360;
        else
            angleOld = cam.transform.eulerAngles.y;
        
        // Calculates new rounded angle
        int section = (int) (Math.Round(angleOld / 60, 0));
        float angleNew = section * 60;

        // Calculates new rounded camPos
        float camPosX = - (float) (Math.Sin(angleNew * (Math.PI / 180)) * camPosRadius);
        float camPosZ = - (float) (Math.Cos(angleNew * (Math.PI / 180)) * camPosRadius);

        // Translates to actual Pos & Rot
        cam.transform.position = new Vector3(camPosX, camPosHeight, camPosZ);
        cam.transform.eulerAngles = new Vector3(camRot.x, angleNew, camRot.z);
    }

    public void ResetView() // When the view has to be reset manually from within Player script
    {
        cam.transform.position = camPos;
        cam.transform.eulerAngles = camRot;
    }
}
