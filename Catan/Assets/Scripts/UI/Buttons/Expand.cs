using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Expand : ButtonTemplate  // To open resources
{
    protected override void Clicked()
    {
        board.playerAtTurn.ShowExpandableResourcesUI(false);
        board.playerAtTurn.ShowExpandedResourcesUI(true);
    }
}
