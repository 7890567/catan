using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThrowDice : ButtonTemplate
{
    public GameObject panelDice;

    protected override void Clicked()
    {
        board.playerAtTurn.Panel(0);
        board.playerAtTurn.ThrowDice();
    }
}
