using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProposalManager : Selectable
{
    // Indicates whether a player has accepted or declined the trade proposal
    public IDictionary<Player, bool> playerList = new Dictionary<Player, bool>();
    private Player acceptedPlayer;

    protected override void Awake()
    {
        // Get all references
        board = FindObjectOfType<Board>();
        confirmButton = GameObject.Find("Button - Confirm");
        confirmScript = confirmButton.GetComponent<Confirm>();

        selectable = this;

        foreach (Player p in board.players)
            playerList.Add(p, false);
    }

    public void PlayerClicked(Player player)
    {
        acceptedPlayer = player;
        Clicked();
    }

    public override void Confirmed()
    {
        if (acceptedPlayer != null)
            board.ProcessPlayerTrade(acceptedPlayer);
        else
            board.ResetTradeInfo();
    }

    public void ResetPanel()
    {
        playerList.Clear();
        
        foreach (Player p in board.players)  // Reset all players
            playerList[p] = false;

        acceptedPlayer = null;
        gameObject.SetActive(false);
    }
}
