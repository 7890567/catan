using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class DiceLarge : DiceRepresentation
{
    public DiceSmall dieSmall;
    public float lerpDuration;
    public Board board;

    public override void ShowEyes(int eyes)
    {
        base.ShowEyes(eyes);    
        StartCoroutine(Lerp(eyes));  // Starts coroutine for lerping purposes
    }

    private IEnumerator Lerp(int eyes)
    {
        float timeElapsed = 0;

        die.color = tempColor;
        tempColor.a = 1f;

        while (timeElapsed < lerpDuration)
        {
            // Fades alpha to 0 (min.) 
            tempColor.a = Mathf.Lerp(1f, 0f, timeElapsed / lerpDuration);
            die.color = tempColor;

            timeElapsed += Time.deltaTime;
            yield return null;
        }
        die.color = tempColor;
        dieSmall.ShowEyes(eyes);
    }
}
