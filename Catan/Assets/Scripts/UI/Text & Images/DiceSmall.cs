using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class DiceSmall : DiceRepresentation
{
    public override void ShowEyes(int eyes)
    {
        base.ShowEyes(eyes);        
        die.color = tempColor;  // alpha becomes 1 (max.)
    }
}