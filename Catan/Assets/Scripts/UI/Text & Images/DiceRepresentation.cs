using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public abstract class DiceRepresentation : MonoBehaviour
{
    public Sprite[] sides = new Sprite[6];  // All  6 sides of a die
    public Image die; 
    protected Color tempColor; 

    protected void Awake()
    {
        die = gameObject.GetComponent<Image>();
        tempColor = die.color;
        // Only possible to assign Vector3 to color property; hence first change Vector3 of color property itself
        tempColor.a = 1f;  // alpha of color is max
    }

    public virtual void ShowEyes(int eyes)
    {
        die.sprite = sides[eyes];
    }
}
