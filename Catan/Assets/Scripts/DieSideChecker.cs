using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class DieSideChecker : MonoBehaviour
{
    // GameObjects of Dice
    public List<GameObject> diceActive = new List<GameObject>();
    // Scripts of Dice
    public List<Die> dice = new List<Die>();    
    private Board board;

    private string fileName;

    private void Awake()
    {
        board = FindObjectOfType<Board>();
        fileName = Application.dataPath + "/Dice_Statistics.csv";
    }

    private void Update()  // Checks if dice are thrown & if they both do not roll anymore
    {
        if (dice.Count != 0 && dice[0] != null && dice[1] != null)
            if (dice[0].rb.IsSleeping() && dice[1].rb.IsSleeping() && dice[0].SideValue != 0 && dice[0].SideValue != 0) 
            {
                board.playerAtTurn.ui.rerollButton.SetActive(false);
                board.playerAtTurn.UpdateDiceUI(dice[0].SideValue, dice[1].SideValue);
                ToFile();

                ClearDie(true);            
            }
    }

    public void ClearDie(bool finalRoll)
    {
        // Reset all measured values
        dice[0].SideValue = 0;
        dice[0].PreviousSideValue = 0;
        dice[1].SideValue = 0;
        dice[1].PreviousSideValue = 0;

        // Delay should be equal to delay 'lerpDuration' from the large Dice
        Destroy(diceActive[0], finalRoll ? board.playerAtTurn.ui.dice[0].lerpDuration : 0);
        Destroy(diceActive[1], finalRoll ? board.playerAtTurn.ui.dice[0].lerpDuration : 0);
        diceActive.Clear();
        dice.Clear();
    }

    private void ToFile()  // Writes thrown number to CSV file for analytic purposes
    {
        try
        {
            TextWriter writer = new StreamWriter(fileName, true);
            string cellContent = (dice[0].SideValue + dice[1].SideValue).ToString();
            writer.WriteLine(cellContent);
            writer.Close();
        }
        catch { Debug.Log("Couldn't write to file"); }
    }

    private void OnTriggerStay(Collider other)  // If the collider of a die enters Table
    {
        Die die = other.transform.parent.gameObject.GetComponent<Die>();
        die.PreviousSideValue = die.SideValue;
        die.SideValue = 7 - int.Parse(other.tag);
    }

    private void OnTriggerExit(Collider other)  // In case the collider has entered last and leaves the Table, which makes it so that the previous collider is the valid one
    {
        Die die = other.transform.parent.gameObject.GetComponent<Die>();
        die.SideValue = die.PreviousSideValue;
    }
}
