# Settlers of Catan - Digital Remake

A digital remake of the Settlers of Catan ([CATAN GmbH 2022](https://www.catan.com/)) played with 4 Players/Rule Based AI on 1 screen.

## Unity Versions

Unity 2020.3 - Disclaimer: We only tested the game in Windows 10/11 and we can **not** guarantee all features are preserved in any other OS.

## How to Open the Game

Locate and download the game from our [Gitlab Repository](./Catan/Build).

Simply double click on the Catan.exe file and enjoy playing the game :)

**When you want to stop playing, you need to press Alt + F4, since the game automatically goes Full Screen.*

## Rules

This Digital Remake closely follows the [rules](./Other files/catan_base_rules_2020_200707.pdf) of the original base game - 'CATAN - Trade Build Settle'.

## Player Controls

Players can use the following controls for game input:

- **Left Mouse** = Selecting
- **Right Mouse** = Panning the Camera around the center of the Board
- **Escape** = Deselecting (when the Confirm Button is active)

---


## How to Open the Project


Download the Project from our [GitLab Repository](https://git.science.uu.nl/7890567/catan.git).

In the Unity Hub click Add.

![Add Project](./Other%20files/Add.png)

Then, go the directory of the Project and click on Catan to select the *Catan* Folder.

![Open Project](./Other%20files/Opening.png)

Now, to open the Project, you simply click on the previously selected Project in the Unity Hub.

## Used Unity Packages

These are Packages that we used and that are not automatically included in a Unity Project:

- 'TextMeshPro' - *Included in Unity Registry*
- '2D Tilemap Editor' - *Included in Unity Registry*
- '2D Tilemap Extras' - *Included in Unity Registry*
- 'Flexible Color Picker' - [Unity Asset Store](https://assetstore.unity.com/packages/tools/gui/flexible-color-picker-150497)

## Setting up a Board

To lay down Tiles, you first need to open the Tile Palette by clicking on the Tiles GameObject under Board in the Hierarchy window.

![Open Tile Palette](./Other%20files/Palette.png)

Then, you can start painting by using the tools from the toolbar and by selecting a Resource from the Tile Pallette and clicking on a Tile in the Grid. Make sure you're using the GameObject brush.

![Painting](./Other%20files/Painting.png)

When you are done with the layout, you can assign Harbors to Intersections adjacent to Water Tiles by going into the Inspector window of Board. There, under *Harbors* you can create a new Harbor by dragging a Water Tile from the Scene to a *New Harbor* and selecting the Resource Type the Harbor will contain. You can also select exactly which Intersections the Harbor is linked to.

![Open Tile Palette](./Other%20files/Harbors.png)

Lastly, you can assign & change Numbers to Tiles by selecting a Tile(that is not Water or Empty) and going to its Tile Script Component. There, under *Number* you can select a number with the slider.

![Tile Number Assignment](./Other%20files/Number.png)

These are the key features to set up a Board, but Board also contains a lot of other features in order to make sure setting up a Board is as easy as possible:

- Randomizing Resources
- Automatically Assigning Tile Numbers
- Changing Resources Types of Tiles through the Inspector
- Changing the Resource Costs of various Buildings

## Dice Statistics

This Project also contains an [Excel sheet](./Other files/Dice_Statistics_Analysis.xlsx) with the Statistical Analysis of all numbers thrown by the Physical Dice. This Statistical Analysis uses data imported from running the game and is meant to be used in order to improve the results of the Physical Dice throwing in this Digital Remake. This way the thrown results will resemble real-world Dice throwing as closely as possible, which is very important in a Game of Chance like Catan.

## Developers

The following students have worked on and contributed to this project:

- Martijn Voordouw @2931044 (m.n.voordouw@students.uu.nl)
- Thomas Rozendaal @0276863 (t.c.rozendaal@students.uu.nl)
- Robin Timmerman @7890567 (r.j.timmerman@students.uu.nl)
- Jasper Hofman @9268057 (j.d.hofman@students.uu.nl)

---


## Reference links to Images that are used in this Project

The list containing references to all UI Images that we did not create:

- [All Arrow Images](https://www.designworkplan.com/read/free-vector-arrows)
- [Settings Image](https://icons-for-free.com/settings+icon-1320183238404656194/)
- [All Die side Images](https://nl.depositphotos.com/vector-images/dice-faces.html?qview=429013456)
- [Playing Cards Image](https://webstockreview.net/image/poker-clipart/2342216.html)
- [Dice Rolling Image](http://clipart-library.com/clipart/dice-cliparts_8.htm)
- [Radio Button Image](https://icon-icons.com/icon/radio-button-off/111068)
- [Player Image](https://icon-icons.com/icon/person/110935)
- [Decline Button Image](https://icon-library.com/icon/x-icon-vector-0.html)
- [All Resource & Development Card Images](https://www.scribd.com/doc/252848213/Cartas-de-Colonos-de-Catan-para-imprimir)

**All of the Images listed above, as stated in their Terms of Use, can be used for Commercial Purposes.*








